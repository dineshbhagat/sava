package org.rajivprab.sava.rest;

import org.rajivprab.sava.logging.Severity;

import javax.ws.rs.core.Response.Status;

import static org.rajivprab.sava.rest.RestUtil.buildResponse;
import static org.rajivprab.sava.rest.RestUtil.buildJson;

/**
 * Extension of DispatchWebAppException, customized for user-inputs attempting to access non-existent resources
 *
 * Created by rajivprab on 6/5/17.
 */
public class ResourceNotFoundException extends LoggableWebAppException {
    public ResourceNotFoundException(String messageAppend) {
        this(messageAppend, null);
    }

    public ResourceNotFoundException(String message, Throwable cause) {
        super(Severity.WARN, message, cause, buildResponse(buildJson(message), Status.NOT_FOUND));
    }
}
