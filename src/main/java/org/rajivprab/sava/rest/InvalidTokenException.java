package org.rajivprab.sava.rest;

import org.rajivprab.sava.logging.Severity;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import static org.rajivprab.sava.rest.RestUtil.buildResponse;
import static org.rajivprab.sava.rest.RestUtil.buildJson;

/**
 * Extension of DispatchWebAppException, customized for invalid-login-tokens
 *
 * Created by rajivprab on 6/5/17.
 */
public class InvalidTokenException extends LoggableWebAppException {
    private static final String DISPLAY_MESSAGE = "Session has timed out. Please log back in";

    public InvalidTokenException(String logMessage) {
        this(logMessage, Severity.INFO);
    }

    public InvalidTokenException(String logMessage, Severity severity) {
        this(logMessage, null, severity);
    }

    public InvalidTokenException(String logMessage, Throwable cause, Severity severity) {
        this(logMessage, cause, severity, DISPLAY_MESSAGE);
    }

    public InvalidTokenException(String logMessage, Throwable cause, Severity severity, String displayMessage) {
        super(severity, logMessage, cause, buildUnauthorizedResponse(displayMessage));
    }

    private static Response buildUnauthorizedResponse(String displayMessage) {
        // Are the headers really needed? If not, just use super.buildResponse()
        // https://stackoverflow.com/questions/9220432/http-401-unauthorized-or-403-forbidden-for-a-disabled-user
        ResponseBuilder response = Response.status(Status.UNAUTHORIZED)
                                           .header(RestUtil.SESSION_HEADER, RestUtil.INVALID_SESSION)
                                           .header(RestUtil.ACCESS_CONTROL_EXPOSE_HEADERS, RestUtil.SESSION_HEADER);
        return buildResponse(response, buildJson(displayMessage));
    }
}
