package org.rajivprab.sava.rest;

import org.json.JSONObject;
import org.rajivprab.cava.Validatec;
import org.rajivprab.sava.database.JooqJson;
import org.rajivprab.sava.logging.LogDispatcher;
import org.rajivprab.sava.logging.Severity;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import java.nio.charset.StandardCharsets;
import java.util.UUID;
import java.util.function.Function;

/**
 * Common utilities for building REST APIs
 * <p>
 * Created by rprabhakar on 2/1/16.
 */
public class RestUtil {
    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String PASSWORD_GRANT_TYPE = "password";
    public static final String DISPLAY_MESSAGE_KEY = "message";

    // TODO Enhancement: What are these for??
    public static final String ACCESS_CONTROL_EXPOSE_HEADERS = "Access-Control-Expose-Headers";
    public static final String SESSION_HEADER = "Session";
    public static final String INVALID_SESSION = "Invalid_Session";

    private static final String VARY_HEADER = "Vary";

    // ----------------- Check Length --------------

    public static void checkLengthMin(String string, int minLength) {
        checkLength(string, minLength, Integer.MAX_VALUE);
    }

    public static void checkLengthMax(String string, int maxLength) {
        checkLength(string, 0, maxLength);
    }

    public static void checkLength(String string, int minInclusive, int maxInclusive) {
        Validatec.notNull(string, IllegalUserInputException.class, "Input is missing");
        Validatec.greaterOrEqual(string.length(), minInclusive, () ->
                new IllegalUserInputException("'" + string + "' should be at least " + minInclusive + " characters"));
        Validatec.greaterOrEqual(maxInclusive, string.length(), () ->
                new IllegalUserInputException("'" + string.substring(0, maxInclusive) +
                                                      "...' should be at most " + maxInclusive + " characters"));
    }

    // -------------- Parse ---------------

    public static UUID parseUUID(String userInput) {
        return parse(userInput, UUID::fromString);
    }

    public static <T> T parse(String userInput, Function<String, T> function) {
        try {
            return function.apply(userInput);
        } catch (Exception e) {
            throw new IllegalUserInputException(e.getMessage(), e);
        }
    }

    // ----------------- Response Error Mapper ----------------

    public static Response mapError(Throwable throwable, LogDispatcher logger) {
        try {
            throw throwable;
        } catch (LoggableWebAppException e) {
            logger.report(e);
            return e.getResponse();
        } catch (WebApplicationException e) {
            // Would be great if this worked, but it doesn't currently, due to bug
            // See DispatchWebAppException class description for more details
            logger.report("ErrorMapper", Severity.ERROR, e);
            return e.getResponse();
        } catch (Throwable th) {
            logger.report("ErrorMapper", Severity.FATAL, th);
            String message = "We're sorry. Something has gone wrong. " +
                    "The error has been logged, and our engineers will take a look as soon as possible.";
            return buildResponse(Response.serverError(), buildJson(message));
        }
    }

    // ----------- Build Response ------------------

    public static JSONObject buildJson(String displayMessage) {
        return new JSONObject().put(DISPLAY_MESSAGE_KEY, displayMessage);
    }

    public static Response buildResponse(JooqJson json) {
        return buildResponse(Response.ok(), json.getJson());
    }

    public static Response buildResponse(JSONObject json) {
        return buildResponse(json, Status.OK);
    }

    public static Response buildResponse(JSONObject json, Status status) {
        return buildResponse(Response.status(status), json);
    }

    // These cache-control responses only return cached results if query/path params, and authorization header, all match
    public static Response buildCachedResponse(JooqJson json, int maxAgeMinutes) {
        return buildResponse(buildCachedResponse(maxAgeMinutes), json.getJson());
    }

    public static Response buildCachedResponse(JSONObject json, int maxAgeMinutes) {
        return buildResponse(RestUtil.buildCachedResponse(maxAgeMinutes), json);
    }

    // Only uses cached data if query/path params, and auth-header, all match
    // https://blog.httpwatch.com/2007/12/10/two-simple-rules-for-http-caching/
    public static ResponseBuilder buildCachedResponse(int maxAgeMinutes) {
        CacheControl cc = new CacheControl();
        cc.setPrivate(false);
        cc.setMaxAge(maxAgeMinutes * 60);
        cc.setNoTransform(false);
        cc.setMustRevalidate(true);
        // http://stackoverflow.com/questions/1700453/caching-proxy-with-authenticated-rest-requests
        // https://www.fastly.com/blog/best-practices-for-using-the-vary-header
        // Without these, users WILL see buggy responses, containing account-info for other users
        // TODO Enhancement - This also implies that different users requesting generic data (eg, /content/all),
        // will not benefit from server-side caching, if they specify the authorization header in their requests.
        // (caching across multiple-reqs for a single user, will still work)
        // Either change the server-response on those requests to ignore the authorization header
        // Or change the client behavior to never send authorization token on requests that aren't user-specific
        return Response.ok().cacheControl(cc).header(VARY_HEADER, AUTHORIZATION_HEADER);
    }

    // ----------------- Helpers --------------

    static Response buildResponse(ResponseBuilder response, JSONObject json) {
        return response.type(MediaType.APPLICATION_JSON_TYPE.withCharset(StandardCharsets.UTF_8.name()))
                       .entity(json.toString()).build();
    }
}
