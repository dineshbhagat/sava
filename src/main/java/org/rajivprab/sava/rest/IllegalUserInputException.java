package org.rajivprab.sava.rest;

import org.rajivprab.sava.logging.Severity;

import javax.ws.rs.core.Response.Status;

import static org.rajivprab.sava.rest.RestUtil.buildResponse;
import static org.rajivprab.sava.rest.RestUtil.buildJson;

/**
 * Extension of DispatchWebAppException, customized for illegal-user-inputs
 *
 * Created by rajivprab on 6/5/17.
 */
public class IllegalUserInputException extends LoggableWebAppException {
    public IllegalUserInputException(String message) {
        this(message, null);
    }

    public IllegalUserInputException(String message, Throwable cause) {
        this(Severity.WARN, message, cause);
    }

    public IllegalUserInputException(Severity severity, String message, Throwable cause) {
        super(severity, message, cause, buildResponse(buildJson(message), Status.BAD_REQUEST));
    }
}
