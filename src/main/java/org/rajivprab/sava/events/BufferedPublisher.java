package org.rajivprab.sava.events;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ListMultimap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;

/**
 * A dummy implementation of Publisher that simply logs and buffers all calls.
 * Intended for testing purposes, but could theoretically be used as a buffer for batching publishes in prod.
 *
 * Not thread safe
 *
 * Created by rajivprab on 6/4/17.
 */
public class BufferedPublisher implements Publisher {
    private static final Logger log = LogManager.getLogger(BufferedPublisher.class);

    private String sendErrorTopic;
    private final ListMultimap<String, Entry> published = ArrayListMultimap.create();

    BufferedPublisher() {}

    @Override
    public Publisher reportErrorsToTopic(String sendErrorTopic) {
        log.info("Setting error-topic to: " + sendErrorTopic);
        this.sendErrorTopic = sendErrorTopic;
        return this;
    }

    public String getSendErrorTopic() {
        return sendErrorTopic;
    }

    @Override
    public void publish(String topic, String title, String message) {
        log.info("Publish invoked on Topic: " + topic + "\nTitle: " + title + "\nMessage:\n" + message);
        published.put(topic, Entry.build(title, message));
    }

    public ListMultimap<String, Entry> getEntries() {
        return ImmutableListMultimap.copyOf(published);
    }

    public void clear() {
        published.clear();
    }

    public static class Entry {
        public final String title;
        public final String message;

        public static Entry build(String title, String message) {
            return new Entry(title, message);
        }

        private Entry(String title, String message) {
            this.title = title;
            this.message = message;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Entry)) return false;
            Entry entry = (Entry) o;
            return Objects.equals(title, entry.title) &&
                    Objects.equals(message, entry.message);
        }

        @Override
        public int hashCode() {
            return Objects.hash(title, message);
        }

        @Override
        public String toString() {
            return "Entry{" +
                    "title='" + title + '\'' +
                    ", message='" + message + '\'' +
                    '}';
        }
    }
}
