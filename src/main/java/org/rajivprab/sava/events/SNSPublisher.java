package org.rajivprab.sava.events;

import com.amazonaws.services.sns.AmazonSNS;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javatuples.Triplet;
import org.rajivprab.cava.Validatec;

import java.util.Optional;

/**
 * Implementation of publisher that uses SNS
 * <p>
 * Created by rajivprab on 6/4/17.
 */
class SNSPublisher implements Publisher {
    private static final Logger log = LogManager.getLogger(Publisher.class);

    private final AmazonSNS snsClient;
    private final Optional<String> reportErrorsToTopic;

    SNSPublisher(AmazonSNS client) {
        this(client, null);
    }

    private SNSPublisher(AmazonSNS client, String reportErrorsToTopic) {
        this.snsClient = client;
        this.reportErrorsToTopic = Optional.ofNullable(reportErrorsToTopic);
    }

    @Override
    public Publisher reportErrorsToTopic(String sendErrorTopic) {
        return new SNSPublisher(snsClient, Validatec.notEmpty(sendErrorTopic));
    }

    @Override
    public void publish(String topic, String title, String message) {
        try {
            rawPublish(topic, title, message);
        } catch (RuntimeException e) {
            Triplet originalMessage = Triplet.with(topic, title, message);
            log.error("Unable to publish on SNS the following Topic/Title/Message: " + originalMessage, e);
            String errorTopic = reportErrorsToTopic.orElseThrow(() -> e);
            try {
                rawPublish(errorTopic, "SNS Publish Error",
                           "Original message: " + originalMessage + "\n" + ExceptionUtils.getStackTrace(e));
            } catch (RuntimeException e2) {
                log.fatal("Error reporting SNS-publish failed as well", e2);
                throw e2;
            }
        }
    }

    // -------------------- Helper --------------------

    private void rawPublish(String topic, String title, String message) {
        title = title.length() > 99 ? title.substring(0, 90) : title;
        title = title.trim().length() < 5 ? title + "_____" : title;
        title = title.replaceAll("[^A-Za-z0-9 ]", "_");
        snsClient.publish(topic, message, title);
    }
}
