package org.rajivprab.sava.events;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;

/**
 * SNS Publishing client. AWS provided client is already very simple for sending specific title/message to a topic.
 * This class does the following in addition:
 * - Stores the SNS-Client, along with credentials
 * - Allows disable flag to be set (for QA/test mode), which will stop sending of any SNS messages entirely
 * - Will send an alert to specified error topic if a SNS publish fails for any reason
 * - Will trim the title and replace any illegal characters, so that AWS doesn't throw an error for that reason
 *
 * Do not use LogPublisher here, since LogPublisher will often use SNSPublisher itself, creating an infinite loop.
 * <p>
 * Created by rprabhakar on 8/31/15.
 */
public interface Publisher {
    static BufferedPublisher getBuffered() {
        return new BufferedPublisher();
    }

    static Publisher getSNS(String accessKey, String secretAccessKey, Regions region) {
        return getSNS(new BasicAWSCredentials(accessKey, secretAccessKey), region);
    }

    static Publisher getSNS(AWSCredentials awsCredentials, Regions region) {
        AmazonSNS sns = AmazonSNSClientBuilder.standard()
                                              .withRegion(region)
                                              .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                                              .build();
        return new SNSPublisher(sns);
    }

    @Deprecated
    static Publisher getSNS(String accessKey, String secretAccessKey) {
        return getSNS(new BasicAWSCredentials(accessKey, secretAccessKey));
    }

    @Deprecated
    static Publisher getSNS(AWSCredentials awsCredentials) {
        return new SNSPublisher(new AmazonSNSClient(awsCredentials));
    }

    // If the publish call encounters an error, a second error-message will be published to the topic specified here
    // Method may return a newly constructed instance of Publisher
    Publisher reportErrorsToTopic(String sendErrorTopic);

    /**
     * Publishes with lengths/illegal-chars automatically corrected.
     * If publish fails, sends a 2nd publish to specified error topic, notifying of the failed publish.
     * If the 2nd publish fails as well, logs the error and then throw an exception.
     */
    void publish(String topic, String title, String message);
}
