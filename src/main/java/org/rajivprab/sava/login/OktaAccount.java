package org.rajivprab.sava.login;

import org.json.JSONObject;
import org.rajivprab.cava.Validatec;

/**
 * Representation of an Okta-Account
 *
 * Created by rajivprab on 7/15/17.
 */
public class OktaAccount {
    private final String loginEmail;
    private final String givenName;
    private final String surName;
    private final String oktaID;

    static OktaAccount parseVerifyResponse(JSONObject responseJson) {
        Validatec.equals(responseJson.getString("status"), "SUCCESS");
        responseJson = responseJson.getJSONObject("_embedded").getJSONObject("user");
        return parseProfile(responseJson.getJSONObject("profile"), responseJson.getString("id"));
    }

    static OktaAccount parseGetUserResponse(JSONObject responseJson) {
        Validatec.equals(responseJson.getString("status"), "ACTIVE");
        return parseProfile(responseJson.getJSONObject("profile"), responseJson.getString("id"));
    }

    static OktaAccount parseCreateResponse(JSONObject responseJson) {
        Validatec.equals(responseJson.getString("status"), "STAGED");
        return parseProfile(responseJson.getJSONObject("profile"), responseJson.getString("id"));
    }

    private static OktaAccount parseProfile(JSONObject responseJson, String oktaID) {
        String loginEmail = responseJson.getString("login");
        String firstName = responseJson.getString("firstName");
        String lastName = responseJson.getString("lastName");
        return new OktaAccount(loginEmail, firstName, lastName, oktaID);
    }

    private OktaAccount(String loginEmail, String givenName, String surName, String oktaID) {
        Validatec.contains(loginEmail, "@");
        this.loginEmail = Validatec.notEmpty(loginEmail);
        this.givenName = Validatec.notEmpty(givenName);
        this.surName = Validatec.notEmpty(surName);
        this.oktaID = Validatec.notEmpty(oktaID);
    }

    public String getLoginEmail() {
        return loginEmail;
    }

    public String getUsername() {
        return loginEmail.split("@")[0];
    }

    public String getGivenName() {
        return givenName;
    }

    public String getSurName() {
        return surName;
    }

    String getOktaID() {
        return oktaID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OktaAccount)) return false;

        OktaAccount that = (OktaAccount) o;

        return loginEmail.equals(that.loginEmail)
                && (givenName != null ? givenName.equals(that.givenName) : that.givenName == null)
                && (surName != null ? surName.equals(that.surName) : that.surName == null);
    }

    @Override
    public int hashCode() {
        int result = loginEmail.hashCode();
        result = 31 * result + (givenName != null ? givenName.hashCode() : 0);
        result = 31 * result + (surName != null ? surName.hashCode() : 0);
        return result;
    }
}
