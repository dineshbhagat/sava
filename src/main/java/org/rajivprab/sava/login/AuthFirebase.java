package org.rajivprab.sava.login;

import com.google.api.client.http.HttpResponseException;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.common.annotations.VisibleForTesting;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import com.google.firebase.auth.UserRecord;
import com.google.firebase.auth.UserRecord.CreateRequest;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.rajivprab.cava.Validatec;
import org.rajivprab.cava.exception.IOExceptionc;
import org.rajivprab.sava.logging.Severity;
import org.rajivprab.sava.rest.LoggableWebAppException;

import javax.ws.rs.core.Response.Status;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.rajivprab.sava.rest.RestUtil.buildJson;
import static org.rajivprab.sava.rest.RestUtil.buildResponse;

/** Very thin wrapper around the FirebaseAuth SDK. Does some exception converting */
public class AuthFirebase {
    public static void init(String databaseName, String certificate) {
        init(databaseName, IOUtils.toInputStream(certificate, StandardCharsets.UTF_8));
    }

    public static void init(String databaseName, InputStream certificate) {
        try {
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(certificate))
                    .setDatabaseUrl("https://" + databaseName + ".firebaseio.com/")
                    .build();
            FirebaseApp.initializeApp(options);
        } catch (IOException e) {
            throw new IOExceptionc(e);
        }
    }

    // TODO Enhancement: How to generate an ID token for QA testing?
    public static FirebaseToken verifyIdToken(String idToken) {
        try {
            return FirebaseAuth.getInstance().verifyIdToken(idToken);
        } catch (IllegalArgumentException e) {
            Validatec.isNull(e.getMessage());
            throw new AuthFirebaseException("Bad firebase-id-token: " + idToken, e);
        } catch (FirebaseAuthException e) {
            if (e.getCause() instanceof IOException &&
                    e.getCause().getMessage().contains("Decoding Firebase ID token failed")) {
                throw new AuthFirebaseException("Bad firebase-id-token: " + idToken, e);
            } else {
                throw wrap(e);
            }
        }
    }

    public static void deleteUser(String userID) {
        try {
            FirebaseAuth.getInstance().deleteUser(userID);
        } catch (FirebaseAuthException e) {
            throw wrap(e);
        }
    }

    public static UserRecord getUserByID(String userID) {
        try {
            return FirebaseAuth.getInstance().getUser(userID);
        } catch (FirebaseAuthException e) {
            throw wrap(e);
        }
    }

    public static UserRecord getUserByEmail(String email) {
        try {
            return FirebaseAuth.getInstance().getUserByEmail(email);
        } catch (FirebaseAuthException e) {
            throw wrap(e);
        }
    }

    // ---------------- Helpers -----------------

    @VisibleForTesting
    static UserRecord createUser(String userID, String email, String password) {
        CreateRequest request = new CreateRequest().setEmail(email).setPassword(password).setUid(userID);
        try {
            return FirebaseAuth.getInstance().createUser(request);
        } catch (FirebaseAuthException e) {
            throw wrap(e);
        }
    }

    private static AuthFirebaseException wrap(FirebaseAuthException e) {
        return new AuthFirebaseException(getSummary(e), e);
    }

    private static String getSummary(FirebaseAuthException e) {
        if (e.getCause() instanceof HttpResponseException) {
            HttpResponseException cause = (HttpResponseException) e.getCause();
            return e.getErrorCode() + ": " +
                    new JSONObject(cause.getContent()).getJSONObject("error").getString("message");
        } else {
            return e.getMessage();
        }
    }

    public static class AuthFirebaseException extends LoggableWebAppException {
        private AuthFirebaseException(String userMessage, Throwable cause) {
            super(Severity.WARN, userMessage, cause, buildResponse(buildJson(userMessage), Status.UNAUTHORIZED));
        }
    }
}
