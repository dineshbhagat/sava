package org.rajivprab.sava.login;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import org.rajivprab.sava.logging.Severity;
import org.rajivprab.sava.rest.LoggableWebAppException;

import javax.ws.rs.core.Response.Status;
import java.util.Collection;

import static org.rajivprab.sava.rest.RestUtil.buildResponse;
import static org.rajivprab.sava.rest.RestUtil.buildJson;

/**
 * LoginException that is thrown by Sava.
 * Is suitable for exposing to users directly, through the underlying WebAppException.
 * Should be logged as well to the dispatcher, in order to generate appropriate alerts.
 *
 * Created by rajivprab on 6/25/17.
 */
public class LoginException extends LoggableWebAppException {
    static final String BLANK_FIELD = "The field cannot be left blank";
    static final String ILLEGAL_USERNAME_MESSAGE = "User name cannot contain '@'";
    static final String USER_NAME_IN_USE_MESSAGE = "login: An object with this field already exists";
    static final String BAD_PASSWORD_MESSAGE = "Authentication failed";
    static final String ILLEGAL_PASSWORD_MESSAGE = "Password requirements were not met";
    static final String BLANK_USERNAME_PASSWORD_MESSAGE = "The 'username' and 'password' attributes are required";
    static final String NOT_FOUND_MESSAGE = "Not found: Resource not found";

    private static final Collection<String> USER_ERROR_MESSAGES = ImmutableList.of(
            ILLEGAL_USERNAME_MESSAGE, BAD_PASSWORD_MESSAGE, ILLEGAL_PASSWORD_MESSAGE, BLANK_FIELD,
            BLANK_USERNAME_PASSWORD_MESSAGE, USER_NAME_IN_USE_MESSAGE, NOT_FOUND_MESSAGE);

    public LoginException(IllegalArgumentException e) {
        this(e, e.getMessage());
    }

    public LoginException(String message) {
        this(null, message);
    }

    public LoginException(Exception cause, String message) {
        super(getSeverity(message), message, cause, buildResponse(buildJson(message), getStatus(message)));
    }

    private static Status getStatus(String message) {
        return isUserError(message) ? Status.UNAUTHORIZED : Status.INTERNAL_SERVER_ERROR;
    }

    private static Severity getSeverity(String message) {
        return isUserError(message) ? Severity.INFO : Severity.FATAL;
    }

    private static boolean isUserError(String responseMessage) {
        responseMessage = Strings.nullToEmpty(responseMessage);
        return USER_ERROR_MESSAGES.stream().anyMatch(responseMessage::contains);
    }
}
