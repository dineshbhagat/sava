package org.rajivprab.sava.fcm;

import com.google.common.collect.ImmutableSet;
import org.json.JSONObject;
import org.rajivprab.cava.JSONObjectImmutable;
import org.rajivprab.cava.Validatec;

import java.util.Collection;
import java.util.Set;

// Immutable
public final class PushNotification {
    private final String title;
    private final String body;
    private final JSONObjectImmutable payload;
    private final ImmutableSet<String> registrationTokens;

    PushNotification(String title, String body, JSONObject payload, Set<String> registrationTokens) {
        this.title = Validatec.notNull(title);
        this.body = Validatec.notNull(body);
        this.payload = JSONObjectImmutable.build(payload);
        this.registrationTokens = ImmutableSet.copyOf(registrationTokens);
    }

    public String title() {
        return title;
    }

    public String body() {
        return body;
    }

    public JSONObject payload() {
        return payload;
    }

    public Collection<String> registrationTokens() {
        return registrationTokens;
    }

    public static PushNotificationBuilder builder() {
        return new PushNotificationBuilder();
    }

    @Override
    public String toString() {
        return "PushNotification{"
                + " \n\ttitle=" + title
                + ",\n\tbody=" + body
                + ",\n\tpayload=" + payload
                + ",\n\tregistrationTokens=" + registrationTokens
                + "\n}";
    }

    @Override
    public boolean equals(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int hashCode() {
        throw new UnsupportedOperationException();
    }

}
