package org.rajivprab.sava.threading;

import com.google.common.collect.ImmutableMap;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.eclipse.jetty.util.thread.ThreadPool;
import org.rajivprab.cava.Validatec;
import org.rajivprab.cava.exception.CheckedExceptionWrapper;
import org.rajivprab.sava.logging.LogDispatcher;
import org.rajivprab.sava.logging.Severity;

import java.time.Duration;
import java.util.Map;

/**
 * Unbounded {@link QueuedThreadPool} together with error logging if we run into backlogs.
 * <p>
 * Primarily intended for use with {@link org.eclipse.jetty.server.Server}'s constructor,
 * which requires a {@link ThreadPool} interface.
 * <p>
 * Created by rprabhakar on 12/22/15.
 */
public class CapacityLoggingThreadPool implements ThreadPoolDelegator {
    private final QueuedThreadPool pool;
    private final LogDispatcher logDispatcher;
    private final CapacityChecker capacityChecker;

    // --------------- Constructors ------------

    public static ThreadPool build(int fixedThreads, LogDispatcher logDispatcher) {
        try {
            QueuedThreadPool pool = new QueuedThreadPool();
            pool.setMaxThreads(fixedThreads);
            pool.setMinThreads(fixedThreads);
            pool.setIdleTimeout((int) Duration.ofDays(1000).toMillis());
            pool.start();
            return build(pool, logDispatcher);
        } catch (Exception e) {
            throw CheckedExceptionWrapper.wrapIfNeeded(e);
        }
    }

    public static ThreadPool build(QueuedThreadPool pool, LogDispatcher logDispatcher) {
        return new CapacityLoggingThreadPool(pool, logDispatcher);
    }

    private CapacityLoggingThreadPool(QueuedThreadPool pool, LogDispatcher logDispatcher) {
        this.pool = pool;
        this.logDispatcher = logDispatcher;
        this.capacityChecker = new ThreadPoolCapacityChecker(pool, logDispatcher);
    }

    // --------------- Public Methods ------------

    @Override
    public ThreadPool getDelegate() {
        return pool;
    }

    @Override
    public void execute(Runnable runnable) {
        try {
            Validatec.isTrue(pool.isStarting() || pool.isStarted(),
                             "Execute failed because pool status is " + pool.getState());
            getDelegate().execute(runnable);
        } catch (Throwable e) {
            // Move into its own ExceptionReportingThreadPool?
            // Or not needed since this only catches notStarted/queueFull errors, and not functional errors?
            logDispatcher.report(this, Severity.FATAL, "Execute exception", e);
            throw e;
        }

        capacityChecker.checkCapacity();
    }

    private static class ThreadPoolCapacityChecker extends CapacityChecker {
        private final QueuedThreadPool pool;
        private final LogDispatcher logDispatcher;

        public ThreadPoolCapacityChecker(QueuedThreadPool pool, LogDispatcher logDispatcher) {
            this.pool = pool;
            this.logDispatcher = logDispatcher;
        }

        @Override
        protected void fireAlert() {
            int queueSizePercentage = 100 * pool.getQueueSize() / pool.getMaxThreads();
            int busyThreadPercentage = getBusyThreadPercentage();
            Severity severity = queueSizePercentage > 10 ? Severity.FATAL : Severity.ERROR;
            Map<String, String> metrics = ImmutableMap.of(
                    "pool-state", pool.getState(),
                    "queueSizePercentage", queueSizePercentage + "",
                    "busyThreadPercentage", busyThreadPercentage + "");
            logDispatcher.report(this, severity, "Capacity alert on ThreadPool: " + metrics.entrySet());
        }

        @Override
        protected boolean atLowCapacity() {
            return getBusyThreadPercentage() > 75;
        }

        private int getBusyThreadPercentage() {
            return 100 * pool.getBusyThreads() / pool.getMaxThreads();
        }
    }
}
