package org.rajivprab.sava.email;

import org.rajivprab.cava.Validatec;

import java.util.Optional;

/**
 * Builder for the EmailInfo object, which is then used by the Email interface
 *
 * Created by rprabhakar on 4/22/16.
 */
public class EmailInfo {
    private final String toEmail;
    private final String title;
    private final String body;
    private final String fromEmail;
    private final String replyToEmail;
    private final String toName;
    private final String fromName;
    private final int suppressionGroup;
    private final String bccEmail;

    private EmailInfo(String toEmail, String title, String body, String fromEmail, String replyToEmail,
                      String toName, String fromName, int suppressionGroup, String bccEmail) {
        Validatec.greaterThan(suppressionGroup, 0);
        this.suppressionGroup = suppressionGroup;
        this.toEmail = Validatec.notEmpty(toEmail);
        this.title = Validatec.notEmpty(title);
        this.body = Validatec.notEmpty(body);
        this.fromEmail = Validatec.notEmpty(fromEmail);
        this.replyToEmail = Validatec.notEmpty(replyToEmail);
        this.toName = Validatec.notNull(toName);
        this.fromName = Validatec.notNull(fromName);
        this.bccEmail = bccEmail;
    }

    // ------------- Getters -----------

    public String getToEmail() {
        return toEmail;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public String getReplyToEmail() {
        return replyToEmail;
    }

    public String getToName() {
        return toName;
    }

    public String getFromName() {
        return fromName;
    }

    public int getSuppressionGroup() {
        return suppressionGroup;
    }

    public Optional<String> getBccEmail() {
        return Optional.ofNullable(bccEmail);
    }

    // --------------

    @Override
    public String toString() {
        return "EmailInfo{" +
                "toEmail='" + toEmail + '\'' +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", fromEmail='" + fromEmail + '\'' +
                ", replyToEmail='" + replyToEmail + '\'' +
                ", toName='" + toName + '\'' +
                ", fromName='" + fromName + '\'' +
                ", suppressionGroup='" + suppressionGroup + '\'' +
                ", bccEmail='" + bccEmail + '\'' +
                '}';
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String toEmail;
        private String title;
        private String body;
        private String fromEmail;
        private String replyToEmail;
        private int suppressionGroup;

        private String toName = "";
        private String fromName = "";
        private String bccEmail = null;

        public Builder setToEmail(String toEmail) {
            this.toEmail = toEmail;
            return this;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setBody(String body) {
            this.body = body;
            return this;
        }

        public Builder setFromEmail(String fromEmail) {
            this.fromEmail = fromEmail;
            if (replyToEmail == null) {
                setReplyToEmail(fromEmail);
            }
            return this;
        }

        public Builder setReplyToEmail(String replyToEmail) {
            this.replyToEmail = replyToEmail;
            return this;
        }

        public Builder setToName(String toName) {
            this.toName = toName;
            return this;
        }

        public Builder setFromName(String fromName) {
            this.fromName = fromName;
            return this;
        }

        public Builder setSuppressionGroup(int suppressionGroup) {
            this.suppressionGroup = suppressionGroup;
            return this;
        }

        public Builder setBccEmail(String bccEmail) {
            this.bccEmail = bccEmail;
            return this;
        }

        public EmailInfo build() {
            return new EmailInfo(toEmail, title, body, fromEmail, replyToEmail,
                                 toName, fromName, suppressionGroup, bccEmail);
        }
    }
}
