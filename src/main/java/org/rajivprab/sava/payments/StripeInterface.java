package org.rajivprab.sava.payments;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.stripe.Stripe;
import com.stripe.exception.*;
import com.stripe.model.*;
import org.apache.commons.lang3.Validate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rajivprab.sava.logging.Severity;
import org.rajivprab.sava.rest.LoggableWebAppException;

import javax.ws.rs.core.Response.Status;
import java.util.Map;

import static org.rajivprab.sava.rest.RestUtil.buildResponse;
import static org.rajivprab.sava.rest.RestUtil.buildJson;

/**
 * Interface for communicating with Stripe.
 * Method specifies exactly what inputs are needed, and deals with construction of parameter map, and SDK calls.
 * Exceptions thrown are also grouped together into generic categories: UserError, StripeError, and APIError.
 * The new exceptions are also WebApplicationExceptions, which allow for a wrapper to directly convert them into a user response.
 * <p>
 * Created by rprabhakar on 12/26/15.
 */
public class StripeInterface {
    private static final Logger log = LogManager.getLogger(StripeInterface.class);

    public StripeInterface(String apiKey) {
        synchronized (StripeInterface.class) {
            Validate.isTrue(Stripe.apiKey == null || Stripe.apiKey.equals(apiKey),
                            "Stripe API-Key conflict");
            if (Stripe.apiKey == null) {
                Stripe.apiKey = apiKey;
            }
        }
    }

    public String createAccount(String email) {
        Map<String, Object> parameters = ImmutableMap.of("managed", false, "email", email);
        Account publisherAccount = runStripeAPI(() -> Account.create(parameters), parameters);
        log.info("Successfully created account: " + publisherAccount);
        return publisherAccount.getId();
    }

    // https://stripe.com/docs/tutorials/charges
    public String createCustomer(String stripeToken, long personID) {
        Map<String, Object> parameters = ImmutableMap.of("source", stripeToken,
                                                         "description", String.valueOf(personID));
        Customer customer = runStripeAPI(() -> Customer.create(parameters), parameters);
        log.info("Successfully created customer: " + customer);
        return customer.getId();
    }

    public String getCustomerToken(String cardNumber, int expiryMonth, int expiryYear, String CVC) {
        Map<String, Object> cardParams = ImmutableMap.of("number", cardNumber,
                                                         "exp_month", expiryMonth,
                                                         "exp_year", expiryYear,
                                                         "cvc", CVC);
        Map<String, Object> tokenParams = ImmutableMap.of("card", cardParams);
        Token token = runStripeAPI(() -> Token.create(tokenParams), tokenParams);
        log.info("Successfully received token: " + token);
        return token.getId();
    }

    public Account getAccount(String publisherID) {
        return runStripeAPI(() -> Account.retrieve(publisherID, null), Maps.newHashMap());
    }

    public Transfer transfer(int numCentsUSD, String destinationID, String description) {
        Map<String, Object> transferParams = ImmutableMap.of("amount", numCentsUSD,
                                                             "currency", "usd",
                                                             "destination", destinationID,
                                                             "description", description);
        Transfer transfer = runStripeAPI(() -> Transfer.create(transferParams), transferParams);
        log.warn("User successfully redeemed cash: " + transfer);
        return transfer;
    }

    public Charge captureCharge(String customerID, int numCents) {
        Map<String, Object> chargeMap = ImmutableMap.of("amount", numCents,
                                                        "currency", "usd",
                                                        "customer", customerID);
        Charge charge = runStripeAPI(() -> Charge.create(chargeMap), chargeMap);
        // TODO Enhancement: If charge is declined, delete the user's billing info
        Validate.isTrue(charge.getCaptured(), "Charge has not been captured already. Need to do so now");
        log.info("Successfully captured charge: " + charge);
        return charge;
    }

    // --- Helper methods ---

    // Params is there purely to facilitate error-logging of input parameters, if something fails
    private static <T> T runStripeAPI(StripeAPICall<T> stripeAPICall, Map<String, Object> params) {
        Validate.notNull(Stripe.apiKey);
        try {
            return stripeAPICall.run();
        } catch (AuthenticationException e) {
            throw new OurStripeException(params, e);
        } catch (InvalidRequestException e) {
            // TODO - This should be moved to caucus repo, not here in sava.
            // Also change GeneralAppException to IllegalUserInputException
            if (e.getMessage().contains("An account with this email already exists")) {
                String userMessage = "You already have a Stripe account, which prevents us from creating one for you. " +
                        "Please contact us and we'll make things right!";
                throw new LoggableWebAppException(Severity.ERROR, e,
                                                  buildResponse(buildJson(userMessage), Status.BAD_REQUEST));
            } else {
                throw new OurStripeException(params, e);
            }
        } catch (APIConnectionException | APIException e) {
            throw new TheirStripeException(params, e);
        } catch (CardException e) {
            throw new UserStripeException(params, e);
        } catch (StripeException e) {
            throw new OurStripeException(params, e);
        }
    }

    private interface StripeAPICall<T> {
        T run() throws StripeException;
    }

    // ---- Exceptions ----
    public static class UserStripeException extends LoggableWebAppException {
        private UserStripeException(Map<String, Object> parameters, CardException e) {
            super(Severity.WARN, "Parameters: " + parameters.entrySet(), e,
                  buildResponse(buildJson("Card Error: " + e.getMessage()), Status.FORBIDDEN));
        }
    }

    public static class OurStripeException extends LoggableWebAppException {
        private static final String ERROR_MESSAGE = "We apologize. We're having problems with our payment processing " +
                "system. We will fix this as soon as possible. Thank you for your patience";

        private OurStripeException(Map<String, Object> parameters, Exception e) {
            super(Severity.FATAL, "Parameters: " + parameters.entrySet(), e,
                  buildResponse(buildJson(ERROR_MESSAGE), Status.INTERNAL_SERVER_ERROR));
        }
    }

    public static class TheirStripeException extends LoggableWebAppException {
        private static final String ERROR_MESSAGE = "Our payments processing server is experiencing issues. " +
                "Please try again later. Thank you for your patience.";

        private TheirStripeException(Map<String, Object> parameters, Exception e) {
            super(Severity.ERROR, "Parameters: " + parameters.entrySet(), e,
                  buildResponse(buildJson(ERROR_MESSAGE), Status.SERVICE_UNAVAILABLE));
        }
    }
}
