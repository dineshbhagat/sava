package org.rajivprab.sava.s3;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.ObjectMetadata;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javatuples.Pair;
import org.rajivprab.cava.IOUtilc;
import org.rajivprab.cava.ThreadUtilc;
import org.rajivprab.cava.Validatec;
import org.rajivprab.cava.exception.CheckedExceptionWrapper;
import org.rajivprab.cava.exception.IOExceptionc;

import java.io.*;
import java.util.concurrent.Callable;

/**
 * Simple implementation of S3Interface.
 * Is thread-safe as long as the provided AmazonS3 is thread-safe.
 * <p>
 * Created by rprabhakar on 7/19/15.
 */
class S3Implementation implements S3Interface {
    private static final Logger log = LogManager.getLogger(S3Implementation.class);
    private static final int NUM_ATTEMPTS = 5;
    private final AmazonS3 amazonS3;

    S3Implementation(AmazonS3 amazonS3) {
        this.amazonS3 = amazonS3;
    }

    @Override
    public String getFullContents(String bucketName, String key) {
        String failMessage = "Failed to read from S3: " + Pair.with(bucketName, key);
        return callWithRetry(() -> IOUtilc.toString(getS3Stream(bucketName, key)), failMessage);
    }

    @Override
    public void downloadFile(String bucketName, String key, File outputFile) {
        try (InputStream s3Stream = getS3Stream(bucketName, key);
             OutputStream fileOutputStream = new BufferedOutputStream(new FileOutputStream(outputFile))) {
            IOUtils.copy(s3Stream, fileOutputStream);
        } catch (IOException e) {
            throw new IOExceptionc(e);
        }
    }

    @Override
    public InputStream getS3Stream(String bucketName, String key) {
        String failMessage = "Failed to get stream from S3: " + Pair.with(bucketName, key);
        InputStream stream = callWithRetry(
                () -> amazonS3.getObject(bucketName, key).getObjectContent(), failMessage);
        return new BufferedInputStream(stream);
    }

    // TODO Enhancement: Set server-side encryption settings?
    @Override
    public void writeToS3(String bucketName, String key, InputStream inputStream) {
        callWithRetry(() -> amazonS3.putObject(bucketName, key, inputStream, new ObjectMetadata()),
                      "Failed to write to S3: " + Pair.with(bucketName, key));
    }

    // Neat bonus: Doesn't actually make a API call to S3. Implemented locally using an AWS library
    @Override
    public String getURL(String bucketName, String key) {
        return amazonS3.getUrl(bucketName, key).toString();
    }

    // ----------------- Helpers --------------

    private static <T> T callWithRetry(Callable<T> task, String failMessage) {
        int iteration = 0;
        AmazonS3Exception caught = null;
        Validatec.greaterThan(NUM_ATTEMPTS, 0);
        while (iteration < NUM_ATTEMPTS) {
            try {
                return task.call();
            } catch (AmazonS3Exception e) {
                log.warn(failMessage, e);
                // TODO Enhancement: Do not retry if bucket/object does not exist, or if access is denied
                iteration++;
                ThreadUtilc.sleep(10);
                caught = e;
            } catch (Exception e) {
                throw CheckedExceptionWrapper.wrapIfNeeded(e);
            }
        }
        throw caught;
    }
}
