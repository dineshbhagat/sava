package org.rajivprab.sava.s3;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

import java.io.File;
import java.io.InputStream;

/**
 * Interface for reading from S3
 *
 * Created by rajivprab on 5/29/17.
 */
public interface S3Interface {
    @Deprecated
    static S3Interface get(AWSCredentialsProvider credentialsProvider) {
        return get(new AmazonS3Client(credentialsProvider));
    }

    static S3Interface get(Regions region) {
        return get(AmazonS3ClientBuilder.standard().withRegion(region).build());
    }

    static S3Interface get(String accessKey, String secretKey, Regions region) {
        return get(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey)), region);
    }

    // Returns a thread-safe instance of S3Interface
    // See: https://stackoverflow.com/a/26381559/4816322
    static S3Interface get(AWSCredentialsProvider credentialsProvider, Regions region) {
        return get(AmazonS3ClientBuilder.standard().withCredentials(credentialsProvider).withRegion(region).build());
    }

    // Is thread-safe as long as the provided amazonS3 is thread-safe
    static S3Interface get(AmazonS3 amazonS3) {
        return new S3Implementation(amazonS3);
    }

    InputStream getS3Stream(String bucketName, String key);

    void writeToS3(String bucketName, String key, InputStream inputStream);

    String getFullContents(String bucketName, String key);

    void downloadFile(String bucketName, String key, File outputFile);

    String getURL(String bucketName, String key);
}
