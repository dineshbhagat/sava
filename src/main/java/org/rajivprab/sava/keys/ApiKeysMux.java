package org.rajivprab.sava.keys;

import com.google.common.collect.ImmutableMap;
import org.rajivprab.cava.DynamicConstant;
import org.rajivprab.cava.Validatec;

import java.util.Map;

/**
 * Returns the specific ApiKeys credentials, corresponding to the environment currently being run in.
 *
 * Is thread-safe, as long as the underlying ApiKeys are all thread-safe.
 */
public class ApiKeysMux implements ApiKeys {
    private final ImmutableMap<String, ApiKeys> environmentKeys;
    private final DynamicConstant<String> environment;

    public static ApiKeysMux build(Map<String, ApiKeys> environmentKeys) {
        return new ApiKeysMux(DynamicConstant.noDefault(), environmentKeys);
    }

    public static ApiKeysMux build(String defaultEnv, Map<String, ApiKeys> environmentKeys) {
        Validatec.contains(environmentKeys.keySet(), defaultEnv);
        return new ApiKeysMux(DynamicConstant.withDefault(defaultEnv), environmentKeys);
    }

    private ApiKeysMux(DynamicConstant<String> environment, Map<String, ApiKeys> environmentKeys) {
        this.environment = environment;
        this.environmentKeys = ImmutableMap.copyOf(environmentKeys);
    }

    public ApiKeysMux setEnvironment(String environment) {
        Validatec.contains(environmentKeys.keySet(), environment);
        this.environment.set(environment);
        return this;
    }

    @Override
    public String getCredentials(String key) {
        return environmentKeys.get(environment.get()).getCredentials(key);
    }
}
