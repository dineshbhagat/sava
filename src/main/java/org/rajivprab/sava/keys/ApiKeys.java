package org.rajivprab.sava.keys;

/**
 * Interface for retrieving credentials from a credentials service
 *
 * Created by rprabhakar on 2/1/16.
 */
public interface ApiKeys {
    String getCredentials(String key);
}
