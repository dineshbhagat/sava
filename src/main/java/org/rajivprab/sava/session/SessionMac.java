package org.rajivprab.sava.session;

import org.rajivprab.sava.login.Crypto;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.util.Arrays;

public interface SessionMac {
    Mac getMacInstance();

    static SessionMac getHmac(byte[] key) {
        return new SessionHmac(key, "HmacSHA1");
    }

    class SessionHmac implements SessionMac {
        byte[] key;
        String algorithm;

        private SessionHmac(byte[] key, String algorithm) {
            this.key = Arrays.copyOf(key, key.length);
            this.algorithm = algorithm;
        }

        // Mac is not thread-safe. Do not try to reuse one instance
        @Override
        public Mac getMacInstance() {
            SecretKeySpec hmacSigningKey = new SecretKeySpec(key, algorithm);
            return Crypto.getMacInstance(algorithm, hmacSigningKey);
        }
    }
}
