package org.rajivprab.sava.session;

public interface SessionCrossChecker<T> {
    // In order to support logout functionality, need to store all logged-out-tokens in some external data-store.
    // This method should then check that data-store, to see if this token has been logged-out previously.
    boolean isTokenRevoked(String token);

    // Any post-decryption verification that you'd like to perform on the data,
    // using your application's invariants or external sources of truth.
    void verifySessionData(T sessionData);
}
