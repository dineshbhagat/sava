package org.rajivprab.sava.session;

import org.rajivprab.sava.login.Crypto;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import java.security.NoSuchAlgorithmException;

public interface SessionEncryption {
    Cipher getCipherInstance() throws NoSuchAlgorithmException, NoSuchPaddingException;
    SecretKey getEncryptionSecretKey();
    int ivNumBytes();

    static SessionEncryption getAes(String password, byte[] salt, int keyNumBits) {
        SecretKey key = Crypto.genEncryptionSecretKey("PBKDF2WithHmacSHA1", password, salt,
                                                      65536, keyNumBits, "AES");
        return new SessionAes("AES/CBC/PKCS5Padding", key, 16);
    }

    class SessionAes implements SessionEncryption {
        private final String getCipherFormat;
        private final SecretKey getEncryptionSecretKey;
        private final int ivNumBytes;

        private SessionAes(String cipherFormat, SecretKey getEncryptionSecretKey, int ivNumBytes) {
            this.getCipherFormat = cipherFormat;
            this.getEncryptionSecretKey = getEncryptionSecretKey;
            this.ivNumBytes = ivNumBytes;
        }

        @Override
        public SecretKey getEncryptionSecretKey() {
            return getEncryptionSecretKey;
        }

        @Override
        public int ivNumBytes() {
            return ivNumBytes;
        }

        // Cipher is not thread-safe. Do not try to reuse one instance
        @Override
        public Cipher getCipherInstance() throws NoSuchAlgorithmException, NoSuchPaddingException {
            return Cipher.getInstance(getCipherFormat);
        }
    }
}
