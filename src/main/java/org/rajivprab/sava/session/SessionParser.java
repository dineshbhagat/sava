package org.rajivprab.sava.session;

import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.SerializationException;
import org.apache.commons.lang3.SerializationUtils;
import org.rajivprab.cava.Validatec;
import org.rajivprab.cava.exception.CheckedExceptionWrapper;
import org.rajivprab.sava.logging.Severity;
import org.rajivprab.sava.rest.InvalidTokenException;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.IvParameterSpec;
import java.io.Serializable;
import java.security.AlgorithmParameters;
import java.security.GeneralSecurityException;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Base64;

/**
 * Functionality:
 * 1. Given a Session object, sign/encrypt/encode it into a String representation
 * 2. Given a String, decode it into a Session object
 *
 * To use this class, simply create a MySessionParser class, extend this one, and implement all the required methods.
 *
 * Token strategy: 14:30 at https://www.youtube.com/watch?v=xgkNe6R4Un0
 * 1. Minimize payload and serialize it
 * 2. Sign payload with HMAC
 * 3. Encrypt payload + signature combo
 * 4. Base64 encode the above encryption
 * 5. Encoded encryption result is the token
 * <p>
 * Created by rprabhakar on 7/1/15.
 */
public class SessionParser<T> {
    public static <T> SessionParser<T> build(SessionMac mac, SessionEncryption encryption,
                                             SessionCrossChecker<T> checker) {
        return new SessionParser<>(mac, encryption, checker);
    }

    private final SessionMac mac;
    private final SessionEncryption encryption;
    private final SessionCrossChecker<T> checker;
    private SessionParser(SessionMac mac, SessionEncryption encryption, SessionCrossChecker<T> checker) {
        this.mac = mac;
        this.encryption = encryption;
        this.checker = checker;
    }

    // -------------- Public Functionality --------------

    public Session<T> parseToken(String token) {
        Session<T> session = parseTokenInsecure(token);
        // TODO Enhancement - Parallelize the following 2 checks
        Validatec.isTrue(!checker.isTokenRevoked(token),
                         () -> new InvalidTokenException("Revoked token being used: " + token, Severity.WARN));
        checker.verifySessionData(session.getData());
        return session;
    }

    // Only checks for token encryption
    // Skips isTokenRevoked() and verifySessionData()
    // Suitable for fast access to non-sensitive information, such as user-name
    public Session<T> parseTokenInsecure(String token) {
        Session<T> session = verifyTokenIntegrity(token);
        Validatec.greaterThan(session.getExpiry(), Instant.now(),
                              InvalidTokenException.class, "Token has expired with time");
        return session;
    }

    public String generateToken(T data, Duration validFor) {
        return generateToken(Session.build(data, validFor));
    }

    // Full flow. Credentials -> Payload -> PayloadWithSignature -> decryptedMessage -> encryptedMessage -> token
    // TODO Enhancement: This algorithm is doing authentication first, and then encrypting both data + authentication
    // This is not optimal. Should do encryption first, and then append the authentication of the encrypted bytes
    // See: http://www.thoughtcrime.org/blog/the-cryptographic-doom-principle/
    // http://crypto.stackexchange.com/questions/202/should-we-mac-then-encrypt-or-encrypt-then-mac?lq=1
    // But this link says the opposite?
    // http://crypto.stackexchange.com/questions/5458/should-we-sign-then-encrypt-or-encrypt-then-sign
    // TODO Enhancement: Deserializing before authenticating leaves you open to serialization hack. Fix
    // Consensus seems to be that we should encrypt first, and then certify with HMAC
    // On parsing, we should read out the last X bytes as being MAC,
    // verify the signature, and then decrypt/deserialize if valid
    public String generateToken(Session<T> session) {
        byte[] payload = SerializationUtils.serialize(session);
        PayloadWithSignature payloadWithSignature = new PayloadWithSignature(payload, mac.getMacInstance());
        byte[] decryptedMessage = SerializationUtils.serialize(payloadWithSignature);
        byte[] encryptedMessage = encrypt(decryptedMessage);
        return Base64.getEncoder().encodeToString(encryptedMessage);
    }

    // ------------------- Helpers --------------------

    private Session<T> verifyTokenIntegrity(String token) {
        Validatec.notEmpty(token, InvalidTokenException.class, "No token provided");
        try {
            byte[] encryptedMessage = Base64.getDecoder().decode(token);
            byte[] decryptedMessage = decrypt(encryptedMessage);
            PayloadWithSignature payloadWithSignature = SerializationUtils.deserialize(decryptedMessage);
            payloadWithSignature.verifyHmacSignature(mac.getMacInstance());
            return SerializationUtils.deserialize(payloadWithSignature.getPayload());
        } catch (IllegalArgumentException | SerializationException | ArrayIndexOutOfBoundsException e) {
            throw new InvalidTokenException("Token decode error", e, Severity.WARN);
        } catch (NullPointerException | NegativeArraySizeException e) {
            throw new InvalidTokenException("Token decode error: NPE", e, Severity.ERROR);
        } catch (NoClassDefFoundError | ClassCastException e) {
            throw new InvalidTokenException("Token decode error: Potential Hack", e, Severity.FATAL);
        }
    }

    private byte[] encrypt(byte[] decryptedMessage) {
        try {
            Cipher cipher = encryption.getCipherInstance();
            cipher.init(Cipher.ENCRYPT_MODE, encryption.getEncryptionSecretKey());
            AlgorithmParameters params = cipher.getParameters();
            byte[] iv = params.getParameterSpec(IvParameterSpec.class).getIV();
            byte[] cipherText = cipher.doFinal(decryptedMessage);
            Validatec.equals(iv.length, encryption.ivNumBytes());
            return ArrayUtils.addAll(iv, cipherText);
        } catch (GeneralSecurityException e) {
            throw CheckedExceptionWrapper.wrapIfNeeded(e);
        }
    }

    private byte[] decrypt(byte[] encryptedMessage) {
        try {
            Validatec.greaterThan(encryptedMessage.length, encryption.ivNumBytes());
            byte[] iv = Arrays.copyOfRange(encryptedMessage, 0, encryption.ivNumBytes());
            byte[] cipherText = Arrays.copyOfRange(encryptedMessage,
                                                   encryption.ivNumBytes(), encryptedMessage.length);
            Cipher cipher = encryption.getCipherInstance();
            cipher.init(Cipher.DECRYPT_MODE, encryption.getEncryptionSecretKey(), new IvParameterSpec(iv));
            return cipher.doFinal(cipherText);
        } catch (Exception e) {
            throw new InvalidTokenException("Token decrypt error: " + Arrays.toString(encryptedMessage), e, Severity.INFO);
        }
    }

    @VisibleForTesting
    static class PayloadWithSignature implements Serializable {
        private final byte[] payload;
        private final byte[] hmacSignature;

        public PayloadWithSignature(byte[] payload, Mac mac) {
            this.payload = payload;
            this.hmacSignature = mac.doFinal(payload);
        }

        public void verifyHmacSignature(Mac mac) {
            PayloadWithSignature ref = new PayloadWithSignature(payload, mac);
            if (!Arrays.equals(ref.hmacSignature, this.hmacSignature)) {
                throw new InvalidTokenException("Token HMAC Signature Invalid", Severity.WARN);
            }
        }

        public byte[] getPayload() {
            return payload;
        }

        @Override
        public String toString() {
            return "Payload:\n" + Arrays.toString(payload) + "\nSignature:\n" + Arrays.toString(hmacSignature);
        }
    }
}
