package org.rajivprab.sava.logging;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by rajivprab on 7/3/17.
 */
class DispatcherImpl {
    // Does Nothing. Logs a warning telling users to define their own implementation
    static class WarningDispatcher implements Dispatcher {
        private static final Logger log = LogManager.getLogger(WarningDispatcher.class);

        static final WarningDispatcher INSTANCE = new WarningDispatcher();
        private WarningDispatcher() {}

        @Override
        public void dispatch(Severity severity, String title, String message) {
            if (severity.compareTo(Severity.INFO) > 0) {
                log.warn("Using WarningDispatcher that ignores all calls. " +
                                 "Recommend setting your own or using Dispatcher.getDummyDispatcher");
            }
        }
    }

    // Does nothing at all
    static class DummyDispatcher implements Dispatcher {
        static final DummyDispatcher INSTANCE = new DummyDispatcher();
        private DummyDispatcher() {}

        @Override
        public void dispatch(Severity severity, String title, String message) {}
    }
}
