package org.rajivprab.sava.logging;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.rajivprab.cava.Validatec;
import org.rajivprab.sava.logging.DispatcherImpl.DummyDispatcher;
import org.rajivprab.sava.logging.DispatcherImpl.WarningDispatcher;

/**
 * Dispatcher interface: Performs different actions for different levels of severity.
 *
 * Created by rajivprab on 7/3/17.
 */
public interface Dispatcher {
    default void dispatch(Severity severity, String message) {
        dispatch(severity, Validatec.notEmpty(message), message);
    }

    default void dispatch(Severity severity, Throwable throwable) {
        // GetStackTrace already includes exception type and message
        dispatch(severity, throwable.toString(), ExceptionUtils.getStackTrace(throwable));
    }

    default void dispatch(Severity severity, String message, Throwable throwable) {
        dispatch(severity, Validatec.notEmpty(message),
                 message + "\n" + ExceptionUtils.getStackTrace(Validate.notNull(throwable)));
    }

    void dispatch(Severity severity, String title, String message);

    // A thread-safe dispatcher implementation that buffers all dispatch calls in memory
    static BufferedDispatcher getBufferedDispatcher() { return new BufferedDispatcher(); }

    // Dummy dispatcher that does nothing at all
    static Dispatcher getDummyDispatcher() { return DummyDispatcher.INSTANCE; }

    // Same as dummy dispatcher, except it logs a warning message, whenever a dispatch is called with severity > INFO
    static Dispatcher getWarningDispatcher() { return WarningDispatcher.INSTANCE; }
}
