package org.rajivprab.sava.logging;

// Trace: Only for the most gratuitous log of any and all activity
// Debug: Information that's generally not needed, except when debugging issues
// Info: Issues that dev-ops/UX teams might want to know. Eg, Anything that should produce 4xx http-response
// Notify: Same as Info, but also generate notifications. Example: All-is-well status checks
// Warn: Suspicious behavior that needs to be investigated. Eg: Tampered tokens. Stormpath timeouts.
// Error: Issues that have exposed errors in our code. Eg, Anything that produces 5xx response due to our fault
// Fatal: Show stopper issues that need to be fixed at 2am in the morning. Eg: Stormpath invalid-API-key errors
public enum Severity {
    TRACE, DEBUG, INFO, NOTIFY, WARN, ERROR, FATAL
}
