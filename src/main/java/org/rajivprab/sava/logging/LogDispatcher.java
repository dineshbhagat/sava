package org.rajivprab.sava.logging;


import com.google.common.base.Strings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rajivprab.cava.Validatec;

import java.util.Objects;

/**
 * Log Interface
 * Will handle logging, but will also send emails/texts/calls where needed
 * <p>
 * Created by rprabhakar on 7/25/15.
 */
public class LogDispatcher {
    private final Dispatcher dispatcher;

    public static LogDispatcher build(Dispatcher dispatcher) {
        return new LogDispatcher(dispatcher);
    }
    private LogDispatcher(Dispatcher dispatcher) {
        this.dispatcher = Objects.requireNonNull(dispatcher);
    }

    // -----------

    public void report(Loggable loggable) {
        report(loggable.logFile(), loggable.severity(), loggable.message(), loggable.throwable());
    }

    // -----------

    public void report(Object logFile, Severity severity, String message) {
        report(logFile.getClass(), severity, message);
    }

    public void report(Class logFile, Severity severity, String message) {
        report(logFile.getName(), severity, message);
    }

    public void report(String logFile, Severity severity, String message) {
        log(logFile, severity, Validatec.notEmpty(message));
        dispatcher.dispatch(severity, message);
    }

    // ------------

    public void report(Object logFile, Severity severity, Throwable throwable) {
        report(logFile.getClass(), severity, throwable);
    }

    public void report(Class logFile, Severity severity, Throwable throwable) {
        report(logFile.getName(), severity, throwable);
    }

    public void report(String logFile, Severity severity, Throwable throwable) {
        log(logFile, severity, "", Validatec.notNull(throwable));
        dispatcher.dispatch(severity, throwable);
    }

    // -----------

    public void report(Object logFile, Severity severity, String message, Throwable throwable) {
        report(logFile.getClass(), severity, message, throwable);
    }

    public void report(Class logFile, Severity severity, String message, Throwable throwable) {
        report(logFile.getName(), severity, message, throwable);
    }

    public void report(String logFile, Severity severity, String message, Throwable throwable) {
        if (throwable == null) {
            report(logFile, severity, message);
        } else if (Strings.isNullOrEmpty(message)) {
            report(logFile, severity, throwable);
        } else {
            log(logFile, severity, message, throwable);
            dispatcher.dispatch(severity, message, throwable);
        }
    }

    // ------------

    private static void log(String logFile, Severity severity, String message, Throwable throwable) {
        Logger log = LogManager.getLogger(logFile);
        switch (severity) {
            case TRACE:
                log.trace(message, throwable);
                break;
            case DEBUG:
                log.debug(message, throwable);
                break;
            case INFO:
            case NOTIFY:
                log.info(message, throwable);
                break;
            case WARN:
                log.warn(message, throwable);
                break;
            case ERROR:
                log.error(message, throwable);
                break;
            case FATAL:
                log.fatal(message, throwable);
                break;
            default:
                throw new IllegalStateException("Found unexpected severity: " + severity);
        }
    }

    private static void log(String logFile, Severity severity, String message) {
        Logger log = LogManager.getLogger(logFile);
        switch (severity) {
            case TRACE:
                log.trace(message);
                break;
            case DEBUG:
                log.debug(message);
                break;
            case INFO:
            case NOTIFY:
                log.info(message);
                break;
            case WARN:
                log.warn(message);
                break;
            case ERROR:
                log.error(message);
                break;
            case FATAL:
                log.fatal(message);
                break;
            default:
                throw new IllegalStateException("Found unexpected severity: " + severity);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LogDispatcher)) return false;

        LogDispatcher that = (LogDispatcher) o;

        return dispatcher.equals(that.dispatcher);
    }

    @Override
    public int hashCode() {
        return dispatcher.hashCode();
    }
}
