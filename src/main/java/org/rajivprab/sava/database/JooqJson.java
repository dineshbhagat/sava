package org.rajivprab.sava.database;

import com.google.common.collect.*;
import com.google.common.primitives.Ints;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.Field;
import org.jooq.Result;
import org.jooq.Table;
import org.jooq.TableField;
import org.json.JSONArray;
import org.json.JSONObject;
import org.mockito.Mockito;
import org.rajivprab.cava.JSONObjectImmutable;
import org.rajivprab.cava.JsonUtilc;
import org.rajivprab.cava.Validatec;

import java.sql.Date;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Representation of data returned by org.jooq.Result#formatJSON().
 * Guaranteed to be immutable. All modification methods will return a new copy.
 * <p>
 * TODO Rename class
 * <p>
 * Created by rprabhakar on 1/31/16.
 */
public class JooqJson implements Iterable<JooqJson> {
    private static final Logger log = LogManager.getLogger(JooqJson.class);

    private static final Set<Class> SUPPORTED_FIELD_TYPES =
            ImmutableSet.of(Boolean.class, Integer.class, String.class, Double.class, Long.class,
                            JSONObject.class, UUID.class, OffsetDateTime.class, Date.class, LocalDate.class);
    private static final String FIELDS_KEY = "fields";
    private static final String RECORDS_KEY = "records";

    // TODO Enhancement: Seems wasteful to store both jsonData and fields, when the latter is a subset of former
    // Consider storing fields & JSONArray of records instead
    // Then we will need a way to reversibly generate JooqJson.toJson(), which requires regenerating fields.toJson()
    // Given that the above is going to happen extremely frequently, perhaps its better to just store redundantly
    private final JSONObjectImmutable jsonData;

    // ------------------------- Constructors ------------------------------

    public static JooqJson merge(JooqJson... results) {
        return merge(ImmutableList.copyOf(results));
    }

    public static JooqJson merge(List<JooqJson> resultsList) {
        Validatec.notEmpty(resultsList);
        resultsList.forEach(entry -> Validatec.isTrue(
                JsonUtilc.equals(entry.getFields(), resultsList.get(0).getFields())));

        JSONArray fields = resultsList.get(0).jsonData.getJSONArray(FIELDS_KEY);
        JSONArray records = new JSONArray();
        resultsList.forEach(entry -> entry.getRecords().forEach(records::put));
        return build(fields, records);
    }

    public static JooqJson build(Result result) {
        return build(result.formatJSON());
    }

    public static JooqJson build(String jsonString) {
        return new JooqJson(JSONObjectImmutable.build(jsonString));
    }

    private static JooqJson build(JSONArray fieldsJson, JSONArray records) {
        return build(new JSONObject().put(FIELDS_KEY, fieldsJson).put(RECORDS_KEY, records));
    }

    public static JooqJson build(JSONObject json) {
        return new JooqJson(JSONObjectImmutable.build(json));
    }

    private JooqJson(JSONObjectImmutable json) {
        jsonData = json;
        JsonUtilc.<JSONArray>getStream(jsonData.getJSONArray(RECORDS_KEY))
                .forEach(entry -> Validatec.equals(jsonData.getJSONArray(FIELDS_KEY).length(), entry.length()));
    }

    // ------------------------- Update -----------------------------

    public JooqJson modify(TableField field, String... newValues) {
        Validatec.length(newValues, getNumEntries());
        JSONObject newJson = new JSONObject(jsonData.toString());
        int column = getColumn(field);
        for (int row = 0; row < newValues.length; row++) {
            newJson.getJSONArray(RECORDS_KEY).getJSONArray(row).put(column, newValues[row]);
        }
        return build(newJson);
    }

    public JooqJson clear(TableField... fields) {
        return clear(Arrays.asList(fields));
    }

    public JooqJson clear(Collection<TableField> fields) {
        JSONObject newJson = new JSONObject(jsonData.toString());
        for (TableField field : fields) {
            int column = getColumn(field);
            JsonUtilc.<JSONArray>getStream(newJson.getJSONArray(RECORDS_KEY)).forEach(row -> row.put(column, ""));
        }
        return build(newJson);
    }

    // ---------------------------- Meta ----------------------------

    public JooqJson getShuffled() {
        List<JSONArray> records = getRecords();
        Collections.shuffle(records);
        return build(jsonData.getJSONArray(FIELDS_KEY), JsonUtilc.mergeAllObjectsIntoArray(records));
    }

    public int getNumEntries() {
        return jsonData.getJSONArray(RECORDS_KEY).length();
    }

    // ---------------------------------- Fields ----------------------------------

    public JSONObject getJson() {
        return jsonData;
    }

    public JSONArray getFields() {
        return jsonData.getJSONArray(FIELDS_KEY);
    }

    public List<String> getTables() {
        return JsonUtilc.<JSONObject>getStream(getFields())
                .map(field -> field.optString("table"))
                .collect(Collectors.toList());
    }

    public List<String> getFieldNames() {
        return JsonUtilc.<JSONObject>getStream(getFields())
                .map(field -> field.getString("name"))
                .collect(Collectors.toList());
    }

    public boolean hasFields(TableField... fields) {
        return Arrays.stream(fields)
                     .allMatch(field -> hasField(field.getTable().getName(), field.getName()));
    }

    private boolean hasField(String table, String fieldName) {
        try {
            Validatec.greaterOrEqual(getColumn(table, fieldName), 0);
            return true;
        } catch (IllegalArgumentException e) {
            if (e.getMessage().matches("Found 0 matches for \\[.*] among fields: .*")) {
                return false;
            }
            throw e;
        }
    }

    // ---------------------------- Strip --------------------------

    public JooqJson strip(Table... tables) {
        return strip(getTableFields(tables));
    }

    public JooqJson strip(Collection<TableField> fields) {
        return stripHelper(getColumns(fields.stream()));
    }

    public JooqJson strip(TableField... fields) {
        return stripHelper(getColumns(Arrays.stream(fields)));
    }

    private JooqJson stripHelper(Collection<Integer> columnsToStrip) {
        Set<Integer> columnsToKeep = IntStream.range(0, getFields().length()).boxed().collect(Collectors.toSet());
        columnsToKeep.removeAll(columnsToStrip);
        Validatec.notEmpty(columnsToKeep);
        return trimHelperIndex(columnsToKeep);
    }

    // --------------------------- Trim --------------------------

    public JooqJson trimTo(Table... tables) {
        return trimTo(getTableFields(tables));
    }

    public JooqJson trimTo(TableField... fields) {
        return trimHelperIndex(getColumns(Arrays.stream(fields)));
    }

    public JooqJson trimTo(Collection<TableField> fields) {
        return trimHelperIndex(getColumns(fields.stream()));
    }

    // TODO Enhancement: Preserve original ordering, by sorting the given list?
    // Ordering of argument-list determines ordering of fields in output
    private JooqJson trimHelperIndex(Collection<Integer> columnsToKeep) {
        List<Integer> sortedColumns = Lists.newArrayList(columnsToKeep);
        Collections.sort(sortedColumns);
        Validatec.isTrue(Ordering.natural().isStrictlyOrdered(sortedColumns));

        JSONArray fieldsJson = new JSONArray();
        sortedColumns.forEach(i -> fieldsJson.put(jsonData.getJSONArray(FIELDS_KEY).getJSONObject(i)));

        JSONArray records = new JSONArray();
        for (JSONArray record : getRecords()) {
            JSONArray filtered = new JSONArray();
            sortedColumns.forEach(i -> filtered.put(record.get(i)));
            records.put(filtered);
        }

        return build(fieldsJson, records);
    }

    // ------------------------- Get Rows --------------------------------

    public <T> JooqJson removeDuplicates(TableField<?, T> key) {
        Set<T> uniqueValues = Sets.newHashSet();
        List<T> allValues = getVal(key);
        List<Integer> uniqueRows = IntStream.range(0, allValues.size())
                                            .filter(index -> uniqueValues.add(allValues.get(index)))
                                            .boxed().collect(ImmutableList.toImmutableList());
        return getRows(uniqueRows);
    }

    public JooqJson getRows(int... rows) {
        return getRows(Ints.asList(rows));
    }

    public JooqJson getRows(Iterable<Integer> rows) {
        JSONArray fieldsJson = jsonData.getJSONArray(FIELDS_KEY);
        JSONArray records = new JSONArray();
        for (int row : rows) {
            Validatec.greaterThan(getNumEntries(), row);
            records.put(jsonData.getJSONArray(RECORDS_KEY).getJSONArray(row));
        }
        return build(fieldsJson, records);
    }

    // ------------------------- Filter -------------------------------

    public <T> JooqJson filter(TableField<?, T> field, Predicate<T> predicate) {
        return filter(field.getTable().getName(), field, predicate);
    }

    public <T> JooqJson filter(Field<T> field, Predicate<T> predicate) {
        return filter(null, field, predicate);
    }

    private <T> JooqJson filter(String table, Field<T> field, Predicate<T> predicate) {
        int column = getColumn(table, field.getName());
        Collection<Integer> rows = IntStream.range(0, getNumEntries())
                                            .filter(row -> predicate.test(getVal(field, row, column)))
                                            .boxed().collect(ImmutableList.toImmutableList());
        return getRows(rows);
    }

    // ------------------------- Get List of Values -------------------------------

    public <T> List<T> getVal(TableField<?, T> field) {
        return getVal(field.getTable().getName(), field);
    }

    public <T> List<T> getVal(Field<T> field) {
        return getVal(null, field);
    }

    private <T> List<T> getVal(String table, Field<T> field) {
        int column = getColumn(table, field.getName());
        return IntStream.range(0, getNumEntries())
                        .mapToObj(row -> getVal(field, row, column))
                        .collect(Collectors.toList());
    }

    // --------------------------- Get Single Value -------------------------------

    public <T> T getOnlyVal(TableField<?, T> field) {
        Validatec.equals(getNumEntries(), 1, "Expecting single record in: " + jsonData);
        return getVal(field, 0);
    }

    public <T> T getOnlyVal(Field<T> field) {
        Validatec.equals(getNumEntries(), 1, "Expecting single record in: " + jsonData);
        return getVal(field, 0);
    }

    // -------------------------- Get Specified Value ----------------------------

    public <T> T getVal(TableField<?, T> field, int row) {
        return getVal(field, row, getColumn(field));
    }

    public <T> T getVal(Field<T> field, int row) {
        // TODO Enhancement: Check to see if Field instanceof TableField, and if so, call the above method directly
        // https://stackoverflow.com/questions/1572322/overloaded-method-selection-based-on-the-parameters-real-type
        return getVal(field, row, getColumn(field));
    }

    // Above getVal methods all run in O(N) time, where N is the number of fields, since they call getColumn()
    // This method runs in O(1) time, since column is explicitly specified as an argument
    // Hence, any repeated calls to getVal, should use this method, and not the above
    private <T> T getVal(Field<T> field, int row, int column) {
        Class type = field.getType();
        Validatec.isTrue(SUPPORTED_FIELD_TYPES.contains(type) || type.isEnum(),
                         () -> getExceptionUnsupportedField(field));
        Object rawValue = jsonData.getJSONArray(RECORDS_KEY).getJSONArray(row).get(column);
        if (rawValue.equals(JSONObject.NULL)) { return null; }
        if (rawValue instanceof Integer) {
            if (type.equals(Double.class)) {
                return (T) Double.valueOf((Integer) rawValue);
            } else if (type.equals(Long.class)) {
                return (T) Long.valueOf((Integer) rawValue);
            }
        } else if (rawValue instanceof String) {
            if (type.equals(UUID.class)) {
                return (T) UUID.fromString((String) rawValue);
            } else if (type.equals(OffsetDateTime.class)) {
                return (T) OffsetDateTime.parse((String) rawValue);
            } else if (type.equals(LocalDate.class)) {
                return (T) LocalDate.parse((String) rawValue);
            } else if (type.equals(Date.class)) {
                // TODO Enhancement: Figure out how to make code-gen use LocalDate instead of Date
                log.warn("Using java.util.Date, which is not recommended. Use a custom-binding to java.time.LocalDate");
                return (T) Date.valueOf(LocalDate.parse((String) rawValue));
            } else if (type.equals(JSONObject.class)) {
                return (T) new JSONObject((String) rawValue);
            } else if (type.isEnum()) {
                try {
                    return (T) parseEnum(type, (String) rawValue);
                } catch (IllegalArgumentException e) {
                    throw new IllegalStateException(
                            String.format("Unable to parse '%s' for field '%s'. Full result:\n%s",
                                          rawValue, field, toString()));
                }
            }
        }
        return (T) rawValue;
    }

    private static Object parseEnum(Class enumClass, String value) {
        return Enum.valueOf(enumClass, value);
    }

    private <T> UnsupportedOperationException getExceptionUnsupportedField(Field<T> field) {
        throw new UnsupportedOperationException(
                String.format("Found field: %s, of type: %s. Supported field-types: %s. Full data:\n%s",
                              field, field.getType(), SUPPORTED_FIELD_TYPES, toString()));
    }

    // ------------------------- Helpers ------------------------------

    private List<Integer> getColumns(Stream<TableField> fields) {
        return fields.map(this::getColumn).collect(ImmutableList.toImmutableList());
    }

    private int getColumn(TableField field) {
        return getColumn(field.getTable().getName(), field.getName());
    }

    private int getColumn(Field field) {
        Validatec.isFalse(field instanceof TableField);
        return getColumn(null, field.getName());
    }

    private int getColumn(String table, String fieldName) {
        Validatec.isTrue(table == null || !table.trim().isEmpty());
        List<String> tables = getTables();
        List<String> fieldNames = getFieldNames();
        List<Integer> indexes = IntStream.range(0, getFields().length())
                                         .filter(i -> fieldNames.get(i).equals(fieldName))
                                         .filter(i -> table == null || tables.get(i).equals(table))
                                         .boxed().collect(ImmutableList.toImmutableList());
        if (indexes.size() != 1) {
            String message = String.format("Found %d matches for [%s, %s] among fields: %s",
                                           indexes.size(), table, fieldName, getFields());
            throw new IllegalArgumentException(message);
        }
        return indexes.get(0);
    }

    private List<JSONArray> getRecords() {
        return Lists.newArrayList(JsonUtilc.<JSONArray>getIterable(jsonData.getJSONArray(RECORDS_KEY)));
    }

    // ----------------------------------------------------------------

    @Override
    public String toString() {
        return jsonData.toString(2);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JooqJson)) return false;

        JooqJson jooqJson = (JooqJson) o;

        return (hashCode() == jooqJson.hashCode()) && JsonUtilc.equals(jsonData, jooqJson.jsonData);
    }

    // jsonData.hashCode wouldn't work, since 2 equivalent jsonDatas may produce different hashcodes.
    // Likely because JSONObject does not override @hashcode?
    // Implementation needs to guarantee that identical jsonData produces identical hashcode.
    // Else, can break collections like HashMap that rely on that property.
    @Override
    public int hashCode() {
        // TODO Enhancement: Find better hashcode
        return getFieldNames().hashCode() + 7 * getNumEntries();
    }

    @Override
    public Iterator<JooqJson> iterator() {
        return new JooqJsonIterator(this);
    }

    private static List<TableField> getTableFields(Table... tables) {
        List<TableField> fields = Lists.newArrayList();
        for (Table table : tables) {
            for (Field field : table.fields()) {
                fields.add(getFakeTableField(table.getName(), field));
            }
        }
        return fields;
    }

    // TODO Enhancement: This is an abomination. Better to make a fake class, but that will have a million methods
    private static TableField getFakeTableField(String tableName, Field field) {
        TableField mockTableField = Mockito.mock(TableField.class);

        Table mockTable = Mockito.mock(Table.class);
        Mockito.when(mockTable.getName()).thenReturn(tableName);
        Mockito.when(mockTableField.getTable()).thenReturn(mockTable);

        Mockito.when(mockTableField.getName()).thenReturn(field.getName());
        Mockito.when(mockTableField.getType()).thenReturn(field.getType());
        return mockTableField;
    }

    private static class JooqJsonIterator implements Iterator<JooqJson> {
        private final JooqJson data;
        private int nextIndexToReturn = 0;

        private JooqJsonIterator(JooqJson data) {
            this.data = data;
        }

        @Override
        public boolean hasNext() {
            return data.getNumEntries() > nextIndexToReturn;
        }

        @Override
        public JooqJson next() {
            Validatec.isTrue(hasNext(), NoSuchElementException.class, "Next should not be called when hasNext == false");
            JooqJson result = data.getRows(nextIndexToReturn);
            nextIndexToReturn++;
            return result;
        }
    }
}
