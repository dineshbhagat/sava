package org.rajivprab.sava.database;

import org.jooq.*;
import org.jooq.impl.DSL;
import org.json.JSONObject;

import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.Types;
import java.util.Objects;

// https://www.jooq.org/doc/3.7/manual/code-generation/custom-data-type-bindings/
// We're binding <T> = Object (unknown JDBC type), and <U> = JSONObject (user type)
public class JooqPostgresJsonBinding implements Binding<Object, JSONObject> {
    // The converter does all the work
    @Override
    public Converter<Object, JSONObject> converter() {
        return new Converter<Object, JSONObject>() {
            @Override
            public JSONObject from(Object t) {
                return t == null ? null : new JSONObject(t.toString());
            }

            @Override
            public Object to(JSONObject u) {
                return u == null || u == JSONObject.NULL ? null : u.toString();
            }

            @Override
            public Class<Object> fromType() {
                return Object.class;
            }

            @Override
            public Class<JSONObject> toType() {
                return JSONObject.class;
            }
        };
    }

    // Rending a bind variable for the binding context's value and casting it to the json type
    @Override
    public void sql(BindingSQLContext<JSONObject> ctx) {
        // Depending on how you generate your SQL, you may need to explicitly distinguish
        // between jOOQ generating bind variables or inlined literals. If so, use this check:
        // ctx.render().paramType() == INLINED
        ctx.render().visit(DSL.val(ctx.convert(converter()).value())).sql("::json");
    }

    // Registering VARCHAR types for JDBC CallableStatement OUT parameters
    @Override
    public void register(BindingRegisterContext<JSONObject> ctx) throws SQLException {
        ctx.statement().registerOutParameter(ctx.index(), Types.VARCHAR);
    }

    // Converting the JSONObject to a String value and setting that on a JDBC PreparedStatement
    @Override
    public void set(BindingSetStatementContext<JSONObject> ctx) throws SQLException {
        ctx.statement().setString(ctx.index(), Objects.toString(ctx.convert(converter()).value(), null));
    }

    // Getting a String value from a JDBC ResultSet and converting that to a JSONObject
    @Override
    public void get(BindingGetResultSetContext<JSONObject> ctx) throws SQLException {
        ctx.convert(converter()).value(ctx.resultSet().getString(ctx.index()));
    }

    // Getting a String value from a JDBC CallableStatement and converting that to a JSONObject
    @Override
    public void get(BindingGetStatementContext<JSONObject> ctx) throws SQLException {
        ctx.convert(converter()).value(ctx.statement().getString(ctx.index()));
    }

    // Setting a value on a JDBC SQLOutput (useful for Oracle OBJECT types)
    @Override
    public void set(BindingSetSQLOutputContext<JSONObject> ctx) throws SQLException {
        throw new SQLFeatureNotSupportedException();
    }

    // Getting a value from a JDBC SQLInput (useful for Oracle OBJECT types)
    @Override
    public void get(BindingGetSQLInputContext<JSONObject> ctx) throws SQLException {
        throw new SQLFeatureNotSupportedException();
    }
}
