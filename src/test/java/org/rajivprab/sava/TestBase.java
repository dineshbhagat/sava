package org.rajivprab.sava;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Regions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.rajivprab.sava.keys.ApiKeys;
import org.rajivprab.sava.keys.ApiKeysS3;
import org.rajivprab.sava.logging.BufferedDispatcher;
import org.rajivprab.sava.logging.Dispatcher;
import org.rajivprab.sava.logging.LogDispatcher;
import org.rajivprab.sava.s3.AwsCredentials;
import org.rajivprab.sava.s3.S3Interface;

/**
 * Test base that sets common test infrastructure
 *
 * Integration tests will only work if you have the following AWS keys set up in your environment.
 * Ie, it will only work for Rajiv.
 * TODO: Create a free AWS account and use that to write a publicly runnable integration test
 *
 * Created by rprabhakar on 1/31/16.
 */
public class TestBase {
    protected static final Logger log = LogManager.getLogger(TestBase.class);
    protected static final BufferedDispatcher DISPATCHER = Dispatcher.getBufferedDispatcher();
    protected static final LogDispatcher LOG_DISPATCHER = LogDispatcher.build(DISPATCHER);

    protected static ApiKeys apiKeys() {
        return Holder.API_KEYS;
    }

    @Before
    public void clearDispatches() {
        DISPATCHER.getDispatches().clear();
    }

    private static class Holder {
        private static final AWSCredentialsProvider CREDENTIALS_PROVIDER =
                AwsCredentials.customEnvironmentVariable(
                        "PERSONAL_S3READONLY_ACCESS_KEY_ID",
                        "PERSONAL_S3READONLY_SECRET_ACCESS_KEY");
        private static final S3Interface S3_INTERFACE = S3Interface.get(CREDENTIALS_PROVIDER, Regions.US_EAST_1);
        static final ApiKeys API_KEYS = ApiKeysS3.build("rajivprab-credentials-qa", S3_INTERFACE);
    }
}
