package org.rajivprab.sava.rest;

import com.google.common.base.Charsets;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.rajivprab.cava.FileUtilc;
import org.rajivprab.cava.JsonUtilc;
import org.rajivprab.sava.TestBase;
import org.rajivprab.sava.database.JooqJson;
import org.rajivprab.sava.logging.Severity;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.UUID;

import static com.google.common.truth.Truth.assertThat;
import static org.rajivprab.sava.rest.RestUtil.buildResponse;

public class RestUtilTest extends TestBase {

    // --------------------- Check Length -----------------

    @Test
    public void checkLength_justRight1() {
        RestUtil.checkLength("1?.*5", 2, 5);
    }

    @Test
    public void checkLengthMax_tooLong() {
        try {
            RestUtil.checkLengthMax("12345", 4);
            Assert.fail("Exception should have been thrown");
        } catch (IllegalUserInputException e) {
            assertThat(e).hasMessageThat().contains("'1234...' should be at most 4 characters");
        }
    }

    @Test
    public void checkLengthMin_tooShort() {
        try {
            RestUtil.checkLengthMin("12345", 6);
            Assert.fail("Exception should have been thrown");
        } catch (IllegalUserInputException e) {
            assertThat(e).hasMessageThat().contains("'12345' should be at least 6 characters");
        }
    }

    @Test
    public void checkLengthMinMax_justRight2() {
        RestUtil.checkLength("12345", 5, 5);
    }

    // --------------------- Parse -----------------

    @Test
    public void parseUUID_allGood() {
        UUID uuid = UUID.randomUUID();
        assertThat(RestUtil.parseUUID(uuid.toString())).isEquivalentAccordingToCompareTo(uuid);
    }

    @Test
    public void parseUUID_illegalFormat_throwIllegalUserInputException() {
        try {
            RestUtil.parseUUID("12a4");
            Assert.fail("Exception should have been thrown");
        } catch (IllegalUserInputException e) {
            assertThat(e).hasMessageThat().contains("Invalid UUID string: 12a4");
            assertThat(e).hasCauseThat().isInstanceOf(IllegalArgumentException.class);
            assertThat(e.getResponse().getStatusInfo()).isEqualTo(Status.BAD_REQUEST);
            assertThat(e.getResponse().getEntity()).isEqualTo(getBody("Invalid UUID string: 12a4"));
        }
    }

    // --------------------- Map Response Error -----------------

    @Test
    public void mapError_DispatchWebAppException() {
        Throwable error = new IllegalUserInputException(Severity.ERROR, "message", new ArithmeticException());
        Response response = RestUtil.mapError(error, LOG_DISPATCHER);

        assertThat(response.getStatusInfo()).isEqualTo(Status.BAD_REQUEST);
        assertThat(response.getEntity()).isEqualTo(getBody("message"));

        assertThat(DISPATCHER.getDispatches()).hasSize(1);
        assertThat(DISPATCHER.getDispatches(Severity.ERROR).keySet()).containsExactly("message");
    }

    private static String getBody(String display) {
        return new JSONObject().put("message", display).toString();
    }

    @Test
    public void mapError_webAppException() {
        Response injected = Response.status(Status.FORBIDDEN).entity("you shall not pass").build();
        Throwable error = new WebApplicationException("internal message", injected);
        Response response = RestUtil.mapError(error, LOG_DISPATCHER);

        assertThat(response).isEqualTo(injected);

        assertThat(DISPATCHER.getDispatches()).hasSize(1);
        assertThat(DISPATCHER.getDispatches(Severity.ERROR).keySet())
                .containsExactly("javax.ws.rs.WebApplicationException: internal message");
    }

    @Test
    public void mapError_randomThrowable() {
        Response response = RestUtil.mapError(new ArithmeticException("divide by 0"), LOG_DISPATCHER);
        assertThat(response.getStatusInfo()).isEqualTo(Status.INTERNAL_SERVER_ERROR);

        String displayText = "We're sorry. Something has gone wrong. The error has been logged, " +
                "and our engineers will take a look as soon as possible.";
        assertThat(response.getEntity()).isEqualTo(getBody(displayText));

        assertThat(DISPATCHER.getDispatches()).hasSize(1);
        assertThat(DISPATCHER.getDispatches(Severity.FATAL).keySet())
                .containsExactly("java.lang.ArithmeticException: divide by 0");
    }

    // --------------------- Build Response -----------------

    private static final String JOOQ_JSON = FileUtilc.readClasspathFile("database/caucus_jooq_1.json");

    @Test
    public void buildResponseJson() {
        Response response = buildResponse(new JSONObject(JOOQ_JSON));
        assertThat(response.getStatusInfo()).isEqualTo(Status.OK);
        assertThat(response.getMediaType())
                .isEqualTo(MediaType.APPLICATION_JSON_TYPE.withCharset(Charsets.UTF_8.name()));
        assertThat(JsonUtilc.equals(new JSONObject((String) response.getEntity()), new JSONObject(JOOQ_JSON))).isTrue();
    }

    @Test
    public void buildResponseJooqJson() {
        Response response = buildResponse(JooqJson.build(JOOQ_JSON));
        checkResponse(response, JooqJson.build(JOOQ_JSON));
    }

    @Test
    public void buildResponseJooqJsonCached() {
        Response response = RestUtil.buildCachedResponse(JooqJson.build(JOOQ_JSON), 10);
        checkResponse(response, JooqJson.build(JOOQ_JSON));
        checkCacheControlHeaders(response, 10);
    }

    @Test
    public void buildResponseJsonCached_shouldBeIdenticalToJooqJson() {
        Response response = RestUtil.buildCachedResponse(new JSONObject(JOOQ_JSON), 10);
        checkResponse(response, JooqJson.build(JOOQ_JSON));
        checkCacheControlHeaders(response, 10);
    }

    // ----------------------------------------

    private static void checkCacheControlHeaders(Response response, int numMinutes) {
        assertThat(response.getHeaderString("Vary")).isEqualTo("Authorization");
        assertThat(response.getHeaderString("Cache-Control"))
                .isEqualTo("must-revalidate, max-age=" + 60 * numMinutes);
    }

    private static void checkResponse(Response response, JooqJson jooqJson) {
        assertThat(response.getStatusInfo()).isEqualTo(Status.OK);
        assertThat(response.getMediaType())
                .isEqualTo(MediaType.APPLICATION_JSON_TYPE.withCharset(Charsets.UTF_8.name()));
        assertThat(JooqJson.build((String) response.getEntity())).isEqualTo(jooqJson);
    }
}
