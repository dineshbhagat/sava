package org.rajivprab.sava.database;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.rajivprab.cava.FileUtilc;
import org.rajivprab.cava.JsonUtilc;
import org.rajivprab.sava.TestBase;

import java.math.BigInteger;
import java.sql.Date;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.google.common.truth.Truth.assertThat;

/**
 * Unit tests for the DataSet structure
 * <p>
 * Created by rprabhakar on 1/31/16.
 */
public class JooqJsonTest extends TestBase {
    private static final String TEST_STRING_1 = FileUtilc.readClasspathFile("database/caucus_jooq_1.json");
    private static final String TEST_STRING_2 = FileUtilc.readClasspathFile("database/caucus_jooq_2.json");
    private static final String TEST_STRING_3 = FileUtilc.readClasspathFile("database/ellie_jooq.json");

    private static final JooqJson TEST_DATA1 = JooqJson.build(TEST_STRING_1);
    private static final JooqJson TEST_DATA2 = JooqJson.build(TEST_STRING_2);
    private static final JooqJson TEST_DATA3 = JooqJson.build(TEST_STRING_3);

    @Test
    public void mergeJsonResults() {
        JooqJson merged = JooqJson.merge(TEST_DATA1, TEST_DATA2);

        assertThat(merged.getNumEntries()).isEqualTo(2);

        assertThat(JsonUtilc.equals(merged.getFields(), TEST_DATA1.getFields())).isTrue();
        assertThat(JsonUtilc.equals(merged.getFields(), TEST_DATA2.getFields())).isTrue();

        assertThat(merged.getVal(DSL.field("name", String.class)).contains("Indus Valley"));
        assertThat(merged.getVal(DSL.field("name", String.class)).contains("Mesoptamaei"));
    }

    @Test
    public void shuffleEntries() {
        JooqJson merged = JooqJson.merge(TEST_DATA1, TEST_DATA2, TEST_DATA1, TEST_DATA2, TEST_DATA1, TEST_DATA2,
                                         TEST_DATA1, TEST_DATA2, TEST_DATA1, TEST_DATA2, TEST_DATA1, TEST_DATA2);
        JooqJson mergedBackup = JooqJson.build(merged.toString());
        JooqJson mergedShuffled = merged.getShuffled();

        assertThat(mergedShuffled.getNumEntries()).isEqualTo(12);

        assertThat(JsonUtilc.equals(mergedShuffled.getFields(), TEST_DATA1.getFields())).isTrue();

        assertThat(mergedShuffled.getVal(DSL.field("constitution", String.class)))
                .containsAllOf("Indus Valley", "Mesoptamaei");

        assertThat(merged).isEqualTo(mergedBackup);
        assertThat(mergedShuffled).isNotEqualTo(mergedBackup);
    }

    @Test
    public void shouldRecoverOriginalJson_afterPrintingAndParsing() {
        assertThat(JsonUtilc.equals(new JSONObject(TEST_DATA1.toString()), new JSONObject(TEST_STRING_1))).isTrue();
        assertThat(JsonUtilc.equals(new JSONObject(TEST_DATA2.toString()), new JSONObject(TEST_STRING_2))).isTrue();
        assertThat(JsonUtilc.equals(new JSONObject(TEST_DATA3.toString()), new JSONObject(TEST_STRING_3))).isTrue();

        assertThat(JooqJson.build(TEST_STRING_1)).isEqualTo(TEST_DATA1);
        assertThat(JooqJson.build(TEST_STRING_2)).isEqualTo(TEST_DATA2);
        assertThat(JooqJson.build(TEST_STRING_3)).isEqualTo(TEST_DATA3);
    }

    // ------------------------------------ Fields ------------------------------------

    @Test
    public void hasFields_hasAllFields_returnTrue() {
        boolean result = TEST_DATA3.hasFields(mockField("event", "id", LocalDate.class),
                                              mockField("account", "id", UUID.class),
                                              mockField("account", "username", String.class));
        assertThat(result).isTrue();
    }

    @Test
    public void hasFields_hasAllButLastField_returnFalse() {
        boolean result = TEST_DATA3.hasFields(mockField("event", "location", LocalDate.class),
                                              mockField("account", "id", UUID.class),
                                              mockField("accountc", "username", String.class));
        assertThat(result).isFalse();
    }

    @Test
    public void hasFields_hasAllButMiddleField_returnFalse() {
        boolean result = TEST_DATA3.hasFields(mockField("event", "location", LocalDate.class),
                                              mockField("account", "idc", UUID.class),
                                              mockField("account", "username", String.class));
        assertThat(result).isFalse();
    }

    @Test
    public void hasFields_hasAllButFirstField_returnFalse() {
        boolean result = TEST_DATA3.hasFields(mockField("eventc", "location", LocalDate.class),
                                              mockField("account", "id", UUID.class),
                                              mockField("account", "username", String.class));
        assertThat(result).isFalse();
    }

    @Test
    public void hasFields_missingAllFields_returnFalse() {
        boolean result = TEST_DATA3.hasFields(mockField("event", "locationc", LocalDate.class),
                                              mockField("account", "idc", UUID.class),
                                              mockField("account", "usernamec", String.class));
        assertThat(result).isFalse();
    }

    @Test
    public void hasFields_fieldGotMultipleMatches_throwError() {
        try {
            TEST_DATA3.hasFields(mockField(null, "id", UUID.class));
            Assert.fail("should have failed");
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().contains("Found 2 matches for [null, id]");
        }
    }

    // ------------------------------------ Iterate ------------------------------------

    @Test
    public void canIterateOverSingleElement() {
        log.info("Test Data1: " + TEST_DATA1);
        assertThat(Iterables.getOnlyElement(TEST_DATA1)
                            .getOnlyVal(DSL.field("aggregate_reputation", Double.class)))
                .isWithin(0.0001).of(100009);
    }

    @Test
    public void canIterateOverEmptyResults() {
        JSONObject json = new JSONObject(TEST_DATA1.toString());
        json.getJSONArray("records").remove(0);
        List<String> entries = StreamSupport.stream(JooqJson.build(json).spliterator(), false)
                                            .map(jooq -> jooq.getOnlyVal(DSL.field("constitution", String.class)))
                                            .collect(Collectors.toList());
        assertThat(entries).isEmpty();
    }

    @Test
    public void canIterateOverMultipleElements() {
        JooqJson merged = JooqJson.merge(TEST_DATA1, TEST_DATA2);
        List<String> entries = StreamSupport.stream(merged.spliterator(), false)
                                            .map(jooq -> jooq.getOnlyVal(DSL.field("constitution", String.class)))
                                            .collect(Collectors.toList());
        assertThat(entries).containsExactly("Indus Valley", "Mesoptamaei").inOrder();
    }

    // ------------------------ Immutable ------------------

    @Test
    public void backdoorChangesToJson_shouldThrowException_noChangesMade() {
        try {
            TEST_DATA1.getJson().put("key", "value");
            Assert.fail("Should have failed");
        } catch (UnsupportedOperationException e) {
            assertThat(e).hasMessageThat().contains("cannot be modified");
        }
        assertThat(JsonUtilc.equals(TEST_DATA1.getJson(), new JSONObject(TEST_STRING_1))).isTrue();
    }

    // ------------------------------ Modify --------------------------

    @Test
    public void modifyValue_typeRelatedToString_shouldNotModifyExistingInstance() {
        // TODO
    }

    @Test
    public void clearMultipleFields_shouldNotModifyExistingInstance() {
        // TODO
    }

    // ------------------------------------ Trim ------------------------------------

    @Test
    public void getRows_shouldReturnNewObjectWithSpecifiedRows_shouldNotModifyOriginal() {
        // TODO
    }

    @Test
    public void removeDuplicates_someRowsDuplicatedOnKey_othersAreNot() {
        // TODO
    }

    // ------------------------------------ Trim ------------------------------------

    @Test
    public void trim_tableArgument_shouldGenerateFakeFields() {
        // TODO
    }

    @Test
    public void trim_noRecordsPresent() {
        JooqJson trimmed = TEST_DATA3.getRows().trimTo(mockField("event", "id", UUID.class));

        assertThat(trimmed.getFieldNames()).containsExactly("id");
        assertThat(trimmed.getTables()).containsExactly("event");
        assertThat(trimmed.getNumEntries()).isEqualTo(0);
    }

    @Test
    public void trim_oneField_withDuplicateName_castToLong() {
        JooqJson trimmed = TEST_DATA3.trimTo(mockField("event", "id", UUID.class));

        assertThat(trimmed.getFieldNames()).containsExactly("id");
        assertThat(trimmed.getTables()).containsExactly("event");

        assertThat(trimmed.getVal(mockField("event", "id", Long.class)))
                .containsExactly(0L);
    }

    @Test
    public void trim_twoFields() {
        JooqJson merged = JooqJson.merge(TEST_DATA1, TEST_DATA2);
        JooqJson trimmed = merged.trimTo(mockField("dummy", "theme", String.class),
                                         mockField("dummy", "constitution", String.class));

        assertThat(trimmed.getFieldNames()).containsExactly("theme", "constitution").inOrder();
        assertThat(trimmed.getTables()).containsExactly("dummy", "dummy").inOrder();

        assertThat(trimmed.getVal(DSL.field("constitution", String.class)))
                .containsExactly("Indus Valley", "Mesoptamaei").inOrder();
    }

    @Test
    public void trim_twoFields_orderReversed_shouldPreserveOriginalOrdering() {
        JooqJson merged = JooqJson.merge(TEST_DATA1, TEST_DATA2);
        JooqJson trimmed = merged.trimTo(Lists.newArrayList(
                mockField("dummy", "constitution", String.class),
                mockField("dummy", "theme", String.class)));

        assertThat(trimmed.getFieldNames()).containsExactly("theme", "constitution").inOrder();

        assertThat(trimmed.getVal(DSL.field("constitution", String.class)))
                .containsExactly("Indus Valley", "Mesoptamaei").inOrder();
    }

    // ------------------------------------ Strip ------------------------------------

    @Test
    public void strip_oneField_withDuplicateName() {
        JooqJson trimmed = TEST_DATA3.strip(mockField("event", "id", UUID.class));

        List<String> expectedFields = TEST_DATA3.getFieldNames();
        expectedFields.remove(0);
        assertThat(trimmed.getFieldNames()).containsExactlyElementsIn(expectedFields).inOrder();
        assertThat(trimmed.getVal(mockField("account", "id", Integer.class)))
                .containsExactly(7);
    }

    @Test
    public void strip_twoFields_orderReversed() {
        JooqJson merged = JooqJson.merge(TEST_DATA2, TEST_DATA1);
        JooqJson trimmed = merged.strip(Lists.newArrayList(
                mockField("dummy", "constitution", String.class),
                mockField("dummy", "theme", String.class)));

        assertThat(trimmed.getFieldNames()).containsExactly("name", "aggregate_reputation").inOrder();
        assertThat(trimmed.getTables()).containsExactly("dummy", "dummy").inOrder();

        assertThat(trimmed.getVal(DSL.field("aggregate_reputation", Integer.class)))
                .containsExactly(100000, 100009).inOrder();
    }

    // ------------------------------------ Filter ------------------------------------

    @Test
    public void filter_matchOnlyOne() {
        JooqJson merged = JooqJson.merge(TEST_DATA1, TEST_DATA2);
        assertThat(merged.filter(mockField("constitution", String.class),
                                 val -> val.equals("Mesoptamaei")))
                .isEqualTo(TEST_DATA2);
    }

    @Test
    public void filter_matchBoth() {
        JooqJson merged = JooqJson.merge(TEST_DATA1, TEST_DATA2);
        assertThat(merged.filter(mockField("dummy", "aggregate_reputation", Integer.class),
                                 val -> val > 10000))
                .isEqualTo(merged);
    }

    @Test
    public void filter_matchNone() {
        JooqJson merged = JooqJson.merge(TEST_DATA1, TEST_DATA2);
        JooqJson filtered = merged.filter(DSL.field("aggregate_reputation", Integer.class), (val) -> val < 10000);
        assertThat(JsonUtilc.equals(filtered.getFields(), merged.getFields())).isTrue();
        assertThat(filtered.getNumEntries()).isEqualTo(0);
    }

    // ---------------------------------- GetVal --------------------------------

    @Test
    public void getVal_noTableNameGiven_uniqueFieldName() {
        JooqJson merged = JooqJson.merge(TEST_DATA1, TEST_DATA2);
        assertThat(merged.getVal(DSL.field("theme", String.class)))
                .containsExactly("hello", "hello").inOrder();
    }

    @Test
    public void getVal_tableNameGiven_uniqueFieldName() {
        JooqJson merged = JooqJson.merge(TEST_DATA1, TEST_DATA1);
        assertThat(merged.getVal(mockField("dummy", "theme", String.class)))
                .containsExactly("hello", "hello").inOrder();
    }

    @Test
    public void getVal_tableNameGiven_tableNameDoesNotMatch() {
        try {
            TEST_DATA1.getVal(mockField("dummy2", "theme", String.class));
            Assert.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().contains("Found 0 matches for [dummy2, theme]");
        }
    }

    @Test
    public void getVal_duplicateFieldNamesPresent() {
        try {
            TEST_DATA3.getVal(DSL.field("id", String.class));
            Assert.fail("Should have failed");
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().contains("Found 2 matches for [null, id]");
        }
    }

    @Test
    public void getVal_duplicateFieldNamesPresent_tableNameGiven() {
        assertThat(TEST_DATA3.getVal(mockField("account", "id", Integer.class)))
                .containsExactly(7);
    }

    // ---------------------- Construct JooqJson with no-table field -------------

    @Test
    public void constructResultContainingFieldWithNoTable_getValue_shouldFindSuccessfully() {
        // TODO
    }

    // ---------------------- Type parsing -------------------

    @Test
    public void getVal_shouldParseStringToUUID() {
        assertThat(TEST_DATA3.getOnlyVal(DSL.field("profile_pic", UUID.class)))
                .isEqualTo(UUID.fromString("a11d4dd5-1000-4cb1-92b3-ca7607d9cc67"));
    }

    @Test
    public void getVal_shouldParseString_toJSONObject() {
        JSONObject val = TEST_DATA3.getOnlyVal(mockField("event", "location", JSONObject.class));
        assertThat(JsonUtilc.equals(val, new JSONObject().put("location", "Singapore"))).isTrue();
    }

    @Test
    public void getVal_shouldParseStringToOffsetDateTime() {
        assertThat(TEST_DATA3.getOnlyVal(DSL.field("finish", OffsetDateTime.class)))
                .isEqualTo(LocalDate.of(2017, 12, 16)
                                    .atTime(18, 13, 43, 451000000)
                                    .atOffset(ZoneOffset.ofHours(-4)));
    }

    @Test
    public void getVal_shouldParseBooleans() {
        assertThat(TEST_DATA3.getOnlyVal(DSL.field("phone_number", Boolean.class))).isTrue();
        assertThat(TEST_DATA3.getOnlyVal(DSL.field("location2", Boolean.class))).isFalse();
    }

    @Test
    public void getValue_shouldParseLocalDate() {
        LocalDate expected = LocalDate.of(1985, 11, 19);
        assertThat(TEST_DATA3.getOnlyVal(DSL.field("greatest-date", LocalDate.class))).isEqualTo(expected);

    }

    @Test
    public void getValue_isNull_shouldReturnListWithNull() {
        List<LocalDate> vals = TEST_DATA3.getVal(DSL.field("nullval", LocalDate.class));
        assertThat(vals).hasSize(1);
        assertThat(vals.get(0)).isNull();
    }

    @Test
    public void getOnlyValue_differentTypes_isNull_shouldReturnNull() {
        assertThat(TEST_DATA3.getOnlyVal(DSL.field("nullval", LocalDate.class))).isNull();
        assertThat(TEST_DATA3.getOnlyVal(DSL.field("nullval", Double.class))).isNull();
        assertThat(TEST_DATA3.getOnlyVal(DSL.field("nullval", Fruit.class))).isNull();
    }

    @Test
    public void getValue_shouldParseSqlDate() {
        Date val = TEST_DATA3.getOnlyVal(DSL.field("greatest-date", Date.class));
        assertThat(val).isEqualTo(Date.valueOf(LocalDate.of(1985, 11, 19)));
    }

    @Test
    public void getValue_shouldParseEnums() {
        assertThat(TEST_DATA3.getOnlyVal(DSL.field("real_name", Fruit.class))).isEqualTo(Fruit.Pear);
    }

    @Test
    public void getValue_parseEnums_badName() {
        try {
            TEST_DATA3.getOnlyVal(DSL.field("email", Fruit.class));
            Assert.fail("Should have hit error");
        } catch (IllegalStateException ignore) {}
    }

    @Test
    public void getValue_unsupportedClass_shouldThrowAppropriateError() {
        try {
            TEST_DATA3.getVal(mockField("account", "id", BigInteger.class));
            Assert.fail("Should have failed");
        } catch (UnsupportedOperationException ignored) {}
    }

    // ---------------------------------------------------

    private static <T> Field<T> mockField(String fieldName, Class<T> type) {
        Field<T> field = Mockito.mock(Field.class);
        Mockito.when(field.getName()).thenReturn(fieldName);
        Mockito.when(field.getType()).thenReturn(type);
        return field;
    }

    private static <T> TableField<?, T> mockField(String tableName, String fieldName, Class<T> type) {
        Table table = Mockito.mock(Table.class);
        Mockito.when(table.getName()).thenReturn(tableName);

        TableField<?, T> field = Mockito.mock(TableField.class);
        Mockito.when(field.getTable()).thenReturn(table);
        Mockito.when(field.getName()).thenReturn(fieldName);
        Mockito.when(field.getType()).thenReturn(type);
        return field;
    }

    private enum Fruit implements EnumType {
        Apple("Apple"), Pear("Pear"), Orange("Orange");

        private final String literal;

        private Fruit(String literal) {
            this.literal = literal;
        }

        @Override
        public Catalog getCatalog() {
            return getSchema() == null ? null : getSchema().getCatalog();
        }

        @Override
        public Schema getSchema() {
            return null;
        }

        @Override
        public String getName() {
            return "fruit";
        }

        @Override
        public String getLiteral() {
            return literal;
        }
    }
}
