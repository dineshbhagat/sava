package org.rajivprab.sava.database;

import com.google.common.collect.Lists;
import org.junit.Test;
import org.mockito.Mockito;
import org.rajivprab.cava.ThreadUtilc;
import org.rajivprab.cava.Validatec;
import org.rajivprab.cava.exception.SQLExceptionc;
import org.rajivprab.sava.TestBase;
import org.rajivprab.sava.logging.Severity;

import java.lang.Thread.State;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static com.google.common.truth.Truth.assertThat;

public class ConcurrentConnectionPoolTest extends TestBase {
    private static final int NUM_CONNECTIONS = 50;
    private static final Duration BUILD_LATENCY = Duration.ofMillis(1000);

    @Test
    public void unableToGetAllConnections_shouldContinueWithAvailableOnes_andLogWarning() {
        ConcurrentConnectionPool pool = ConcurrentConnectionPool.build(
                LOG_DISPATCHER, new FakeConnectionFactory(10), 11);
        Mockito.verifyZeroInteractions(pool.leech());
        ThreadUtilc.sleep(200);
        assertThat(DISPATCHER.getDispatches(Severity.FATAL)).hasSize(0);
        assertThat(DISPATCHER.getDispatches(Severity.WARN).entrySet()).hasSize(1);
        assertThat(DISPATCHER.getDispatches(Severity.WARN).keySet().iterator().next())
                // Can return either 9 or 10 due to race-conditions
                .contains("Cannot create new connection. Present pool size: ");
        assertThat(pool.leech()).isNotNull();
    }

    @Test
    public void unableToGetMostConnections_shouldContinueWithAvailableOnes_andLogFatal() {
        ConcurrentConnectionPool pool = ConcurrentConnectionPool.build(
                LOG_DISPATCHER, new FakeConnectionFactory(5), 12);
        Mockito.verifyZeroInteractions(pool.leech());
        ThreadUtilc.sleep(100);
        assertThat(DISPATCHER.getDispatches(Severity.FATAL)).isNotEmpty();
        assertThat(DISPATCHER.getDispatches(Severity.WARN)).isEmpty();
        assertThat(DISPATCHER.getDispatches(Severity.FATAL).keySet().iterator().next())
                // Can return either 4 or 5 due to race-conditions
                .contains("Cannot create new connection. Present pool size: ");
        assertThat(pool.leech()).isNotNull();
    }

    @Test
    public void almostGotAllConnections_shouldWaitSlightly() {
        Duration excessiveTimeout = Duration.ofSeconds(10);
        FakeConnectionFactory factory = new FakeConnectionFactory(NUM_CONNECTIONS);

        ConcurrentConnectionPool pool = ConcurrentConnectionPool.build(LOG_DISPATCHER, factory, NUM_CONNECTIONS);
        Duration closeLatency = getRuntime(() -> pool.closeAllConnections(excessiveTimeout));

        assertThat(closeLatency).isLessThan(excessiveTimeout.dividedBy(5));
        checkAllClosed(factory);
    }

    @Test
    public void gotAllConnections_shouldCloseAllImmediately() {
        FakeConnectionFactory factory = new FakeConnectionFactory(NUM_CONNECTIONS);
        ConcurrentConnectionPool pool = ConcurrentConnectionPool.build(LOG_DISPATCHER, factory, NUM_CONNECTIONS);
        ThreadUtilc.sleep(BUILD_LATENCY);

        Duration excessiveTimeout = Duration.ofSeconds(100);
        Duration closeLatency = getRuntime(() -> pool.closeAllConnections(excessiveTimeout));

        assertThat(closeLatency).isLessThan(excessiveTimeout.dividedBy(20));
        checkAllClosed(factory);
    }

    @Test
    public void unableToGetAllConnections_shouldCloseAllQuickly_thenWaitForTimeout() {
        FakeConnectionFactory factory = new FakeConnectionFactory(50);
        ConcurrentConnectionPool pool = ConcurrentConnectionPool.build(LOG_DISPATCHER, factory, 51);
        ThreadUtilc.sleep(BUILD_LATENCY);  // Give time for all connections to be built

        Duration timeout = Duration.ofSeconds(3);
        Thread closer = new Thread(() -> pool.closeAllConnections(timeout));
        closer.start();
        assertThat(closer.getState()).isEqualTo(State.RUNNABLE);

        // We close all connections very quickly, but continue waiting for the 11th connection to show up
        ThreadUtilc.sleep(timeout.dividedBy(10));
        assertThat(closer.getState()).isAnyOf(State.BLOCKED, State.RUNNABLE, State.TIMED_WAITING);
        checkAllClosed(factory);

        // We continue waiting until just before the timeout
        ThreadUtilc.sleep(timeout.multipliedBy(8).dividedBy(10));
        assertThat(closer.getState()).isAnyOf(State.BLOCKED, State.RUNNABLE, State.TIMED_WAITING);

        // Now we hit the timeout, and the thread exits
        ThreadUtilc.sleep(timeout.multipliedBy(3).dividedBy(10));
        assertThat(closer.getState()).isEqualTo(State.TERMINATED);
    }

    private static void checkAllClosed(FakeConnectionFactory factory) {
        factory.generated.forEach(connection -> {
            try {
                Mockito.verify(connection, Mockito.atLeastOnce()).isClosed();
                Mockito.verify(connection, Mockito.atLeastOnce()).close();
            } catch (SQLException e) {
                throw new SQLExceptionc(e);
            }
        });
    }

    private static Duration getRuntime(Runnable runnable) {
        long controlStart = System.nanoTime();
        long start = System.nanoTime();
        runnable.run();
        long end = System.nanoTime();
        return Duration.ofNanos((end - start) - (start - controlStart));
    }

    private static class FakeConnectionFactory extends ConnectionFactory {
        private final List<Connection> generated = Lists.newCopyOnWriteArrayList();
        private final AtomicInteger numConnectionsRemaining;

        public FakeConnectionFactory(int maxConnections) {
            super("", 1, "", 1, "", "", "");
            numConnectionsRemaining = new AtomicInteger(maxConnections);
        }

        @Override
        public synchronized Connection createNewConnection() {
            Validatec.greaterThan(numConnectionsRemaining.decrementAndGet(), -1,
                                  () -> new SQLExceptionc(new SQLException("Hit limit on connections")));
            Connection connection = Mockito.mock(Connection.class);
            generated.add(connection);
            return connection;
        }
    }
}
