package org.rajivprab.sava.events;

import com.google.common.truth.Truth;
import org.junit.Assert;
import org.junit.Test;
import org.rajivprab.sava.TestBase;
import org.rajivprab.sava.events.BufferedPublisher.Entry;

public class BufferedPublisherTest extends TestBase {
    @Test
    public void bufferedPublisher_setGetErrorTopic() {
        BufferedPublisher publisher = Publisher.getBuffered();
        publisher.reportErrorsToTopic("my_error_topic");
        Truth.assertThat(publisher.getSendErrorTopic()).isEqualTo("my_error_topic");
    }

    @Test
    public void bufferedPublisher_modifyMapDirectly_shouldFail() {
        try {
            Publisher.getBuffered().getEntries().clear();
            Assert.fail("Should have failed");
        } catch (UnsupportedOperationException e) {
            Truth.assertThat(e).hasMessageThat().isNull();
        }
    }

    @Test
    public void bufferedPublisher_publish_getEntries_andClear() {
        BufferedPublisher publisher = Publisher.getBuffered();

        publisher.publish("my topic", "title 1", "message 1");
        publisher.publish("my topic", "title 2", "message 2");
        publisher.publish("other topic", "title 3", "message 3");

        Truth.assertThat(publisher.getEntries()).hasSize(3);
        Truth.assertThat(publisher.getEntries().get("my topic"))
             .containsExactly(Entry.build("title 1", "message 1"),
                              Entry.build("title 2", "message 2"))
             .inOrder();
        Truth.assertThat(publisher.getEntries().get("other topic"))
             .containsExactly(Entry.build("title 3", "message 3"));

        publisher.clear();
        Truth.assertThat(publisher.getEntries()).isEmpty();
    }
}
