package org.rajivprab.sava.login;

import com.amazonaws.util.StringInputStream;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import org.apache.commons.lang3.RandomStringUtils;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.rajivprab.sava.TestBase;
import org.rajivprab.sava.login.AuthFirebase.AuthFirebaseException;

import javax.ws.rs.core.Response.Status;

import static com.google.common.truth.Truth.assertThat;

public class AuthFirebaseIT extends TestBase {
    // Get from https://console.firebase.google.com/u/0/project/sava-testing/authentication/users
    // If accidentally deleted, create a new one for testing purposes, by commenting out the "deleteUser" call below
    private static final String EXISTING_USER_ID = "Cf31Wsr60i";
    private static final String EXISTING_EMAIL = "whackri+sava_testing_firebase_cf31wsr60i@gmail.com";

    @BeforeClass
    public static void setup() throws Exception {
        String credentials = apiKeys().getCredentials("firebase/sava-testing-firebase-adminsdk-jdri6.json");
        AuthFirebase.init("sava-testing", new StringInputStream(credentials));
    }

    @Test
    public void getExistingUserById() {
        assertThat(AuthFirebase.getUserByID(EXISTING_USER_ID).getEmail()).isEqualTo(EXISTING_EMAIL);
    }

    @Test
    public void getExistingUserByEmail() {
        assertThat(AuthFirebase.getUserByEmail(EXISTING_EMAIL).getUid()).isEqualTo(EXISTING_USER_ID);
    }

    @Test
    public void invalidUserId_shouldThrowFirebaseAuthException() {
        checkUserNotFound(EXISTING_USER_ID.toLowerCase());
    }

    // --------------- Create/Delete Users ------------

    @Test
    public void create_get_andDelete_newUser() throws Exception {
        String password = RandomStringUtils.randomAlphanumeric(10);
        String userID = RandomStringUtils.randomAlphanumeric(10);
        String email = "whackri+sava_testing_firebase_" + userID + "@gmail.com";
        UserRecord createUserRecord = AuthFirebase.createUser(userID, email, password);

        assertThat(createUserRecord.getEmail()).isEqualTo(email.toLowerCase());
        assertThat(createUserRecord.getUid()).isEqualTo(userID);
        assertThat(createUserRecord.isDisabled()).isFalse();
        assertThat(createUserRecord.isEmailVerified()).isFalse();

        // TODO Enhancement - Unable to create an ID-token, for use in the verify methods
        // Following guide doesn't work. There's no getCurrentUser method available:
        // https://firebase.google.com/docs/auth/android/start/
        // Using createCustomToken(userID) produces a token that's incompatible with verifyIdToken

        verifyEqual(AuthFirebase.getUserByID(userID), createUserRecord);
        verifyEqual(AuthFirebase.getUserByEmail(email), createUserRecord);

        AuthFirebase.deleteUser(userID);
        checkUserNotFound(userID);
    }

    @Test
    public void deleteExistingUser_andCreateAgain() {
        AuthFirebase.deleteUser(EXISTING_USER_ID);

        String password = RandomStringUtils.randomAlphanumeric(10);
        AuthFirebase.createUser(EXISTING_USER_ID, EXISTING_EMAIL, password);

        getExistingUserByEmail();
        getExistingUserById();
    }

    @Test
    public void create_reuseExistingUserName_shouldFail() {
        try {
            AuthFirebase.createUser(EXISTING_USER_ID, "thecaucusnet@gmail.com", "password");
            Assert.fail("Should have failed");
        } catch (AuthFirebaseException e) {
            checkAuthFirebaseException(e, FirebaseAuthException.class, "uid-already-exists: DUPLICATE_LOCAL_ID");
        }
    }

    @Test
    public void create_reuseExistingEmail_shouldFail() {
        try {
            AuthFirebase.createUser("thecaucusnet", EXISTING_EMAIL, "password");
            Assert.fail("Should have failed");
        } catch (AuthFirebaseException e) {
            checkAuthFirebaseException(e, FirebaseAuthException.class, "email-already-exists: EMAIL_EXISTS");
        }
    }

    // ----------------- Verify -----------------

    @Test
    public void verify_veryMalformedToken_shouldThrowExecException_withCauseIllegalArgumentException() {
        try {
            AuthFirebase.verifyIdToken("123");
            Assert.fail("Should have failed");
        } catch (AuthFirebaseException e) {
            checkAuthFirebaseException(e, IllegalArgumentException.class, "Bad firebase-id-token: 123");
        }
    }

    @Test
    public void verify_slightlyMalformedToken_shouldThrowExecException_withCauseIllegalArgumentException() {
        try {
            AuthFirebase.verifyIdToken("123.abc.def");
            Assert.fail("Should have failed");
        } catch (AuthFirebaseException e) {
            checkAuthFirebaseException(e, FirebaseAuthException.class, "Bad firebase-id-token: 123.abc.def");
        }
    }

    // ------------------- helpers ----------

    private static void checkUserNotFound(String userID) {
        try {
            AuthFirebase.getUserByID(userID);
            Assert.fail("Exception should have been thrown");
        } catch (AuthFirebaseException e) {
            checkAuthFirebaseException(e, FirebaseAuthException.class,
                                       "No user record found for the provided user ID: " + userID);
        }
    }

    private static void checkAuthFirebaseException(AuthFirebaseException e, Class cause, String message) {
        assertThat(e.getCause()).isInstanceOf(cause);
        assertThat(e.getMessage()).isEqualTo(message);
        assertThat(e.getResponse().getStatusInfo()).isEqualTo(Status.UNAUTHORIZED);
        assertThat(e.getResponse().getEntity()).isEqualTo(new JSONObject().put("message", message).toString());
    }

    private static void verifyEqual(UserRecord actual, UserRecord expected) {
        assertThat(actual.getEmail()).isEqualTo(expected.getEmail());
        assertThat(actual.getUid()).isEqualTo(expected.getUid());
        assertThat(actual.isDisabled()).isEqualTo(expected.isDisabled());
        assertThat(actual.isEmailVerified()).isEqualTo(expected.isEmailVerified());
    }
}
