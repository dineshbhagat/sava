package org.rajivprab.sava.login;

import com.google.common.truth.Truth;
import org.apache.commons.lang3.RandomStringUtils;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.rajivprab.sava.TestBase;
import org.rajivprab.sava.logging.Severity;

import javax.ws.rs.core.Response.Status;
import java.util.Arrays;
import java.util.Random;

/**
 * Integration tests for the Okta service. Relies on the credentials in S3.
 *
 * Okta api-token expires every month! Will need to renew continuously, or find other solution.
 * Hence test marked as ignored. Okta no longer actively supported by sava.
 * It's a piece-of-shit, and users should really upgrade to firebase.
 *
 * Created by rajivprab on 6/25/17.
 */
@Ignore
public class OktaIT extends TestBase {
    private static final Random RNG = new Random();

    @Test
    public void getUser() {
        for (TestAccount account : TEST_ACCOUNTS) {
            OktaAccount getAccount = OktaHolder.OKTA.getUser(account.username);
            Truth.assertThat(getAccount.getGivenName()).isEqualTo(account.givenName);
        }
    }

    @Test
    public void createUsers_shouldFailWithError_accountsAlreadyExist() {
        for (TestAccount account : TEST_ACCOUNTS) {
            checkBadCreateUser(account, LoginException.USER_NAME_IN_USE_MESSAGE);
        }
    }

    @Test
    public void createUser_onlyEmailAndPassword_login_resetPassword() {
        TestAccount expected = genRandomAccount();
        OktaAccount actual = OktaHolder.OKTA.createAccount(expected.email, expected.password);

        expected.loginEmail = expected.email;
        expected.username = expected.email.split("@")[0];
        expected.givenName = expected.username;
        expected.surName = "The Great";

        checkAccountLoginAndResetPassword(expected, actual);
    }

    @Test
    public void createUser_onlyUsernamePassword_login_resetPassword() {
        TestAccount expected = genRandomAccount();
        OktaAccount actual = OktaHolder.OKTA.createAccountWithNoEmail(expected.username, expected.password);

        expected.loginEmail = Okta.getFakeLogin(expected.username);
        expected.email = expected.loginEmail;
        expected.givenName = expected.username;
        expected.surName = "The Great";

        checkAccountLoginAndResetPassword(expected, actual);
    }

    @Test
    public void resetPassword_userDoesNotExist_throwsException() {
        checkBadResetPassword(RandomStringUtils.randomAlphabetic(10));
    }

    @Test
    public void resetPassword_goodUsernameBadDomain_throwsException() {
        checkBadResetPassword(TEST_ACCOUNTS[0].username + "@yahoo.com");
    }

    private static void checkBadResetPassword(String reset) {
        try {
            OktaHolder.OKTA.resetPassword(reset);
            Assert.fail();
        } catch (LoginException e) {
            LOG_DISPATCHER.report(e);
            Truth.assertThat(DISPATCHER.getDispatches(Severity.FATAL)).isEmpty();
            Truth.assertThat(e).hasMessageThat().contains(LoginException.NOT_FOUND_MESSAGE);
            Truth.assertThat(e.getResponse().getEntity().toString()).contains(LoginException.NOT_FOUND_MESSAGE);
            Truth.assertThat(e.getResponse().getStatus()).isEqualTo(Status.UNAUTHORIZED.getStatusCode());
        }
    }

    @Test
    public void createAccount_BlankUserName_throwsLoginException() {
        TestAccount account = genRandomAccount();
        account.username = "";
        checkBadCreateUser(account, LoginException.BLANK_FIELD);
    }

    @Test
    public void createAccount_userNameIsEmail_throwsLoginException() {
        TestAccount account = genRandomAccount();
        account.username = "mybad@gmail.com";
        checkBadCreateUser(account, LoginException.ILLEGAL_USERNAME_MESSAGE);
    }

    @Test
    public void createAccount_blankEmail_throwsLoginException() {
        TestAccount account = genRandomAccount();
        account.email = "";
        checkBadCreateUser(account, LoginException.BLANK_FIELD);
    }

    @Test
    public void createAccount_blankNames_throwsLoginException() {
        TestAccount account = genRandomAccount();
        account.givenName = "";
        account.surName = "";
        checkBadCreateUser(account, LoginException.BLANK_FIELD);
    }

    @Test
    public void createAccount_blankPassword_throwsLoginException() {
        TestAccount account = genRandomAccount();
        account.password = "";
        checkBadCreateUser(account, LoginException.BLANK_FIELD);
    }

    @Test
    public void createAccount_simplePassword_throwsLoginException() {
        TestAccount account = genRandomAccount();
        account.password = "123";
        checkBadCreateUser(account, LoginException.ILLEGAL_PASSWORD_MESSAGE);
    }

    @Test
    public void verifyAccountWithLogin() throws Exception {
        Arrays.stream(TEST_ACCOUNTS).forEach(OktaIT::checkGoodLogin);
    }

    @Test
    public void verifyAccountWithEmail_throwsLoginException() throws Exception {
        checkBadLogin(TEST_ACCOUNTS[0].email, TEST_ACCOUNTS[0].password);
    }

    @Test
    public void verifyAccountBadUserName_throwsLoginException() {
        checkBadLogin(TEST_ACCOUNTS[0].username + "bad", TEST_ACCOUNTS[0].password);
    }

    @Test
    public void verifyAccount_GoodUserNameBadDomain_throwsLoginException() {
        checkBadLogin(TEST_ACCOUNTS[0].username + "@baddomain.com", TEST_ACCOUNTS[0].password);
    }

    @Test
    public void verifyAccountBadPassword_throwsLoginException() {
        checkBadLogin(TEST_ACCOUNTS[0].username, TEST_ACCOUNTS[1].password);
    }

    @Test
    public void verifyAccountBlankUserName_throwsLoginException() {
        checkBadLogin("", TEST_ACCOUNTS[1].password, LoginException.BLANK_USERNAME_PASSWORD_MESSAGE);
    }

    @Test
    public void verifyAccountBlankPassword_throwsLoginException() {
        checkBadLogin(TEST_ACCOUNTS[1].username, "", LoginException.BLANK_USERNAME_PASSWORD_MESSAGE);
    }

    // @Test
    @Ignore
    public void listGroups() {
        LOG_DISPATCHER.report(this, Severity.INFO, OktaHolder.OKTA.listGroups().toString());
    }

    private static void checkAccountLoginAndResetPassword(TestAccount expected, OktaAccount actual) {
        Truth.assertThat(actual.getGivenName()).isEqualTo(expected.givenName);
        Truth.assertThat(actual.getSurName()).isEqualTo(expected.surName);
        Truth.assertThat(actual.getUsername()).isEqualTo(expected.username);
        Truth.assertThat(actual.getLoginEmail()).isEqualTo(expected.loginEmail);

        checkBadLogin(expected.username, expected.password + "a");
        checkGoodLogin(expected);

        OktaHolder.OKTA.resetPassword(expected.loginEmail);
        OktaHolder.OKTA.resetPassword(expected.username);
    }

    private static void checkBadCreateUser(TestAccount account, String message) {
        try {
            OktaHolder.OKTA.createAccount(account.username, account.email, account.password,
                                          account.givenName, account.surName);
            Assert.fail();
        } catch (LoginException e) {
            Truth.assertThat(e).hasMessageThat().contains(message);
            Truth.assertThat(e.getResponse().getEntity().toString()).contains(message);
            Truth.assertThat(e.getResponse().getStatusInfo()).isEqualTo(Status.UNAUTHORIZED);
            LOG_DISPATCHER.report(e);
            Truth.assertThat(DISPATCHER.getDispatches(Severity.FATAL)).isEmpty();
        }
    }

    private static void checkGoodLogin(TestAccount account) {
        OktaAccount oktaAccount1 = OktaHolder.OKTA.verifyAccount(account.loginEmail, account.password);
        OktaAccount oktaAccount2 = OktaHolder.OKTA.verifyAccount(account.username, account.password);
        Truth.assertThat(oktaAccount1).isEqualTo(oktaAccount2);
        Truth.assertThat(oktaAccount1.getLoginEmail()).isEqualTo(account.loginEmail);
        Truth.assertThat(oktaAccount1.getUsername()).isEqualTo(account.username);
        Truth.assertThat(oktaAccount1.getGivenName()).isEqualTo(account.givenName);
        Truth.assertThat(oktaAccount1.getSurName()).isEqualTo(account.surName);
    }

    private static void checkBadLogin(String login, String password) {
        checkBadLogin(login, password, LoginException.BAD_PASSWORD_MESSAGE);
    }

    private static void checkBadLogin(String login, String password, String message) {
        try {
            OktaHolder.OKTA.verifyAccount(login, password);
            Assert.fail();
        } catch (LoginException e) {
            Truth.assertThat(e).hasMessageThat().contains(message);
            Truth.assertThat(e.getResponse().getEntity().toString()).contains(message);
            Truth.assertThat(e.getResponse().getStatus()).isEqualTo(Status.UNAUTHORIZED.getStatusCode());
            LOG_DISPATCHER.report(e);
            Truth.assertThat(DISPATCHER.getDispatches(Severity.FATAL)).hasSize(0);
        }
    }

    private static TestAccount genRandomAccount() {
        String email = "thecaucusnet+testing_" + RandomStringUtils.randomAlphabetic(10) + "@gmail.com";
        String loginEmail = RNG.nextBoolean() ?
                email : Okta.getFakeLogin("user_" + RandomStringUtils.randomAlphanumeric(10));
        return new TestAccount(loginEmail.split("@")[0],
                               loginEmail,
                               email,
                               "Pass_1" + RandomStringUtils.randomAlphanumeric(20),
                               "first_name_" + RandomStringUtils.randomAlphabetic(10),
                               "last_name" + RandomStringUtils.randomAlphabetic(10));
    }

    private static class TestAccount {
        String givenName;
        String surName;
        String username;
        String loginEmail;
        String email;
        String password;

        public TestAccount(String username, String loginEmail, String email,
                           String password, String givenName, String surName) {
            this.username = username;
            this.loginEmail = loginEmail;
            this.email = email;
            this.password = password;
            this.givenName = givenName;
            this.surName = surName;
        }

        @Override
        public String toString() {
            return "MyAccount{" +
                    "givenName='" + givenName + '\'' +
                    ", surName='" + surName + '\'' +
                    ", userName='" + username + '\'' +
                    ", email='" + email + '\'' +
                    ", password='" + password + '\'' +
                    '}';
        }
    }

    private static class OktaHolder {
        private static final JSONObject CREDENTIALS = new JSONObject(apiKeys().getCredentials("okta/okta.json"));
        // API Token Derived from Okta -> Dashboard -> Security -> API -> Tokens
        private static final Okta OKTA = new Okta(CREDENTIALS.getString("url"),
                                                  CREDENTIALS.getString("api_token"),
                                                  CREDENTIALS.getString("group_id"));
    }

    private static final TestAccount[] TEST_ACCOUNTS = {
            new TestAccount("testAccountOnlyForTesting10",
                            Okta.getFakeLogin("testAccountOnlyForTesting10"),
                            "thecaucusnet+testingonly10@gmail.com",
                            "testPasswordOnly1Testing",
                            "Tt",
                            "To"),
            new TestAccount("testAccountOnlyForTesting12",
                            Okta.getFakeLogin("testAccountOnlyForTesting12"),
                            "thecaucusnet+testingonly12@gmail.com",
                            "testPasswordOnly2Testing",
                            "Tt",
                            "To"),
            new TestAccount("testAccountOnlyForTesting13",
                            Okta.getFakeLogin("testAccountOnlyForTesting13"),
                            "thecaucusnet+testingonly13@gmail.com",
                            "testPasswordOnly3Testing",
                            "Tt",
                            "To"),
            new TestAccount("testAccountOnlyForTesting14",
                            Okta.getFakeLogin("testAccountOnlyForTesting14"),
                            "thecaucusnet+testingonly14@gmail.com",
                            "testPasswordOnly4Testing",
                            "Tt",
                            "To")};
}
