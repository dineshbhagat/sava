package org.rajivprab.sava.login;

import org.json.JSONObject;
import org.junit.Test;
import org.rajivprab.sava.TestBase;
import org.rajivprab.sava.logging.Severity;

import javax.ws.rs.core.Response.Status;

import static com.google.common.truth.Truth.assertThat;

/**
 * Unit tests for the LoginException class
 *
 * Created by rajivprab on 7/3/17.
 */
public class LoginExceptionTest extends TestBase {
    @Test
    public void shouldConstructExceptionSuccessfully_withExceptionCauseAndMessage() {
        String message = LoginException.USER_NAME_IN_USE_MESSAGE;
        IllegalArgumentException causeException = new IllegalArgumentException(message);
        LoginException loginException = new LoginException(causeException);

        assertThat(loginException.getResponse().getStatusInfo()).isEqualTo(Status.UNAUTHORIZED);
        assertThat(loginException.getResponse().getEntity()).isEqualTo(getBody(message));
        assertThat(loginException.getMessage()).isEqualTo(message);
        assertThat(loginException.toString()).containsMatch("LoginException: " + message + ".*401.*Unauth");

        assertThat(DISPATCHER.getDispatches(Severity.INFO)).isEmpty();
        LOG_DISPATCHER.report(loginException);
        assertThat(DISPATCHER.getDispatches(Severity.INFO)).containsKey(message);
        assertThat(DISPATCHER.getDispatches(Severity.INFO).get(message)).contains(message);
        assertThat(DISPATCHER.getDispatches(Severity.INFO).get(message))
                .contains("at org.rajivprab.sava.login.LoginExceptionTest");
    }

    @Test
    public void shouldConstructExceptionSuccessfully_withCustomMessageAndException() {
        String message = LoginException.ILLEGAL_USERNAME_MESSAGE;
        Exception causeException = new IllegalArgumentException("random-exception-msg");
        LoginException loginException = new LoginException(causeException, message);

        assertThat(loginException.getResponse().getStatusInfo()).isEqualTo(Status.UNAUTHORIZED);
        assertThat(loginException.getResponse().getEntity()).isEqualTo(getBody(message));
        assertThat(loginException.getMessage()).isEqualTo(message);
        assertThat(loginException.toString()).containsMatch("LoginException: " + message + ".*401.*Unauth");

        assertThat(DISPATCHER.getDispatches(Severity.INFO)).isEmpty();
        LOG_DISPATCHER.report(loginException);
        assertThat(DISPATCHER.getDispatches(Severity.INFO)).containsKey(message);
        assertThat(DISPATCHER.getDispatches(Severity.INFO).get(message)).contains(message);
        assertThat(DISPATCHER.getDispatches(Severity.INFO).get(message))
                .contains("at org.rajivprab.sava.login.LoginExceptionTest");
    }

    @Test
    public void shouldConstructExceptionSuccessfully_WithMessageOnly_logFatal() {
        LoginException loginException = new LoginException("abc");
        assertThat(loginException.getResponse().getStatusInfo()).isEqualTo(Status.INTERNAL_SERVER_ERROR);
        assertThat(loginException.getResponse().getEntity()).isEqualTo(getBody("abc"));

        assertThat(DISPATCHER.getDispatches(Severity.FATAL)).isEmpty();
        LOG_DISPATCHER.report(loginException);
        assertThat(DISPATCHER.getDispatches(Severity.FATAL)).containsKey("abc");
        assertThat(DISPATCHER.getDispatches(Severity.FATAL).get("abc")).contains("abc");
        assertThat(DISPATCHER.getDispatches(Severity.FATAL).get("abc")).contains("Internal Server Error");
        assertThat(DISPATCHER.getDispatches(Severity.FATAL).get("abc"))
                .contains("at org.rajivprab.sava.login.LoginExceptionTest");
    }

    private static String getBody(String displayMessage) {
        return new JSONObject().put("message", displayMessage).toString();
    }
}
