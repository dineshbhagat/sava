package org.rajivprab.sava.session;

import com.google.common.collect.Sets;
import com.google.common.truth.Truth;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.Validate;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.rajivprab.cava.Validatec;
import org.rajivprab.sava.TestBase;
import org.rajivprab.sava.rest.LoggableWebAppException;
import org.rajivprab.sava.rest.InvalidTokenException;
import org.rajivprab.sava.rest.RestUtil;
import org.rajivprab.sava.session.SessionParser.PayloadWithSignature;
import org.rajivprab.sava.threading.ThreadHelper;

import java.io.Serializable;
import java.time.Duration;
import java.time.Instant;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

/**
 * Integration tests for credentials and encryption/decryption algorithms
 * Token life extension, revoke check etc touches database. Hence why they are ITs
 * <p>
 * Created by rprabhakar on 7/1/15.
 */
public class SessionTest extends TestBase {
    private static final Random RANDOM = new Random();
    private static final SessionMac MAC = SessionMac.getHmac(EncryptionKeysHolder.HMAC_KEY);
    private static final TestChecker CHECKER = new TestChecker();
    private static final SessionEncryption ENCRYPTION = SessionEncryption.getAes(
            EncryptionKeysHolder.PASSWORD, EncryptionKeysHolder.SALT, EncryptionKeysHolder.KEY_NUM_BYTES * 8);
    private static final SessionParser<MyAccount> PARSER = SessionParser.build(MAC, ENCRYPTION, CHECKER);

    // Often triggers HeapSpace error
    @Ignore
    public void tokenGenerationParseStressTest() {
        IntStream.range(0, 25).forEachOrdered(i -> massSuccessAndFailures());
    }

    @Test
    public void tokenShouldFailAfterExpiry() throws Exception {
        Instant expiry = Instant.now().plusMillis(10);
        Session<MyAccount> session = getRandomSession(expiry);
        String token = PARSER.generateToken(session);
        Thread.sleep(11);
        try {
            PARSER.parseToken(token);
            Assert.fail("Token should fail after expiry");
        } catch (InvalidTokenException e) {
            checkInvalidTokenException(e);
        }
    }

    @Test
    public void tokenShouldSucceedRightBeforeExpiry() throws Exception {
        Instant expiry = Instant.now().plusSeconds(7);
        Session<MyAccount> session = getRandomSession(expiry);
        String token = PARSER.generateToken(session);
        Session<MyAccount> parsedSession = PARSER.parseToken(token);
        Assert.assertEquals(session, parsedSession);
    }

    @Test
    public void massSuccessAndFailures() {
        ThreadHelper helper = ThreadHelper.build(Executors.newFixedThreadPool(2), true);
        helper.execute(this::massSuccess);
        helper.execute(this::massFailures);
        helper.checkTasks();
    }

    @Test
    public void massSuccess() {
        IntStream.range(0, 50).parallel().forEach(i -> tokenGenerationAndParseShouldMatch());
    }

    @Test
    public void massFailures() {
        IntStream.range(0, 8).parallel().forEach(i -> tokenParse_tamperedToken_shouldThrowInvalidTokenException());
    }

    @Test
    public void tokenGenerationAndParseShouldMatch() {
        Session<MyAccount> randomSession = getRandomSession();
        String token = PARSER.generateToken(randomSession);

        Truth.assertThat(token).doesNotContain(randomSession.getData().userName);
        Truth.assertThat(token).doesNotContain(randomSession.getData().email);
        Truth.assertThat(token).doesNotContain(String.valueOf(randomSession.getData().userID));

        MyAccount parsedAccount = PARSER.parseTokenInsecure(token).getData();
        Truth.assertThat(randomSession.getData()).isEqualTo(parsedAccount);
    }

    @Test
    public void shouldGetSameHmacSignature() {
        byte[] randomPayload = RandomStringUtils.random(200).getBytes();
        PayloadWithSignature payloadWithSignature = new PayloadWithSignature(randomPayload, MAC.getMacInstance());
        payloadWithSignature.verifyHmacSignature(MAC.getMacInstance());
    }

    @Test
    public void shouldFailHmacSignatureCheck() {
        byte[] randomPayload = RandomStringUtils.random(200).getBytes();
        PayloadWithSignature payloadWithSignature = new PayloadWithSignature(randomPayload, MAC.getMacInstance());
        payloadWithSignature.verifyHmacSignature(MAC.getMacInstance());

        try {
            randomPayload[7]++;
            payloadWithSignature.verifyHmacSignature(MAC.getMacInstance());
            Assert.fail("Payload changed but signature was not. Should have failed");
        } catch (InvalidTokenException e) {
            checkInvalidTokenException(e);
        }
    }

    @Test
    public void tokenParse_tamperedToken_shouldThrowInvalidTokenException() {
        byte[] tokenBytes = PARSER.generateToken(getRandomSession()).getBytes();
        // Use odd-number of bit-flips, to avoid 2 bit-flips canceling out one another
        IntStream.range(0, RANDOM.nextBoolean() ? 1 : 3).forEach(i -> flipRandomBit(tokenBytes));
        try {
            MyAccount parsedAccount = PARSER.parseTokenInsecure(new String(tokenBytes)).getData();
            Assert.fail("Authentication should have failed: " + parsedAccount);
        } catch (InvalidTokenException e) {
            checkInvalidTokenException(e);
        }
    }

    @Test
    public void tokenGenerationShouldGoQuick() throws InvalidTokenException {
        // Latency falls off exponentially with higher num-experiments. Cache hit optimization?
        long numExperiments = 3;
        Duration aggregateLatency = Duration.ZERO;
        for (int i = 0; i < numExperiments; i++) {
            Session<MyAccount> randomSession = getRandomSession();
            aggregateLatency = aggregateLatency.plus(getLatency(() -> PARSER.generateToken(randomSession)));
        }
        Duration averageLatency = aggregateLatency.dividedBy(numExperiments);
        Truth.assertThat(averageLatency).isAtMost(Duration.ofMillis(20));
    }

    @Test
    public void tokenParsingShouldGoQuick() throws InvalidTokenException {
        long numExperiments = 10;
        Duration aggregateLatency = Duration.ZERO;
        for (int i = 0; i < numExperiments; i++) {
            Session<MyAccount> randomSession = getRandomSession();
            String token = PARSER.generateToken(randomSession);
            aggregateLatency = aggregateLatency.plus(getLatency(() -> PARSER.parseTokenInsecure(token)));
        }
        Duration averageLatency = aggregateLatency.dividedBy(numExperiments);
        Truth.assertThat(averageLatency).isAtMost(Duration.ofMillis(20));
    }

    @Test
    public void shouldFailOnTokenRevoke_butNotOnInsecureParse() {
        Session<MyAccount> randomSession = getRandomSession();
        String token = PARSER.generateToken(randomSession);
        CHECKER.addRevokedToken(token);
        Truth.assertThat(PARSER.parseTokenInsecure(token)).isEqualTo(randomSession);
        try {
            PARSER.parseToken(token);
            Assert.fail();
        } catch (InvalidTokenException ignored) {
            Truth.assertThat(ignored).hasMessageThat().contains("Revoked token being used");
        }
    }

    @Test
    public void shouldFailOnBadSessionData_butNotOnInsecureParse() {
        Session<MyAccount> session = Session.build(
                new MyAccount("username_too_long", "email", 123), Duration.ofDays(1));
        String token = PARSER.generateToken(session);
        Truth.assertThat(PARSER.parseTokenInsecure(token)).isEqualTo(session);
        try {
            PARSER.parseToken(token);
            Assert.fail();
        } catch (ArithmeticException ignored) {
        }
    }

    @Test
    public void shouldFailOnExpiredToken_forAllParsing() {
        Session<MyAccount> session = Session.build(getRandomAccount(), Instant.now());
        String token = PARSER.generateToken(session);
        try {
            PARSER.parseTokenInsecure(token);
            Assert.fail();
        } catch (InvalidTokenException ignored) {
            Truth.assertThat(ignored).hasMessageThat().contains("Token has expired with time");
        }
        try {
            PARSER.parseToken(token);
            Assert.fail();
        } catch (InvalidTokenException ignored) {
            Truth.assertThat(ignored).hasMessageThat().contains("Token has expired with time");
        }
    }

    // -------------------------------------------------

    private static void flipRandomBit(byte[] tokenBytes) {
        int randomIndex;
        byte corruptedByte;

        do {
            randomIndex = RANDOM.nextInt(tokenBytes.length - 4);    // Some aliasing can happen in last few chars
            int randomBit = 1 << RANDOM.nextInt(8);
            corruptedByte = (byte) (tokenBytes[randomIndex] ^ randomBit);
            Assert.assertNotEquals(corruptedByte, tokenBytes[randomIndex]);
        } while (!String.valueOf((char) corruptedByte).matches("[a-zA-Z0-9]"));
        tokenBytes[randomIndex] = corruptedByte;
    }

    private static void checkInvalidTokenException(LoggableWebAppException e) {
        Truth.assertThat(e.getResponse().getEntity().toString()).contains("Please log back in");
        Truth.assertThat(e.getResponse().getHeaderString(RestUtil.SESSION_HEADER)).isEqualTo(RestUtil.INVALID_SESSION);
    }

    private static Session<MyAccount> getRandomSession() {
        return getRandomSession(Instant.now().plus(Duration.ofDays(14)));
    }

    private static Session<MyAccount> getRandomSession(Instant expiry) {
        return Session.build(getRandomAccount(), expiry);
    }

    private static MyAccount getRandomAccount() {
        long userID = Math.abs(RANDOM.nextLong());
        String userName = RandomStringUtils.randomAlphanumeric(10);
        String userEmail = RandomStringUtils.randomAlphanumeric(10);
        return new MyAccount(userName, userEmail, userID);
    }

    private static Duration getLatency(Runnable runnable) {
        long start = System.nanoTime();
        runnable.run();
        long end = System.nanoTime();
        return Duration.ofNanos(end - start).minus(getStartStopLatency());
    }

    private static Duration getStartStopLatency() {
        long start = System.nanoTime();
        long end = System.nanoTime();
        return Duration.ofNanos(end - start);
    }

    private static class MyAccount implements Serializable {
        private final String userName, email;
        private final long userID;

        public MyAccount(String userName, String email, long userID) {
            Validate.notNull(userName);
            Validate.notNull(email);
            Validate.isTrue(userID > 0, "Cannot have negative userID");
            this.userName = userName;
            this.email = email;
            this.userID = userID;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            MyAccount that = (MyAccount) o;
            return userID == that.userID && email.equals(that.email) && userName.equals(that.userName);
        }

        @Override
        public int hashCode() {
            int result = userName.hashCode();
            result = 31 * result + email.hashCode();
            result = 31 * result + (int) (userID ^ (userID >>> 32));
            return result;
        }
    }

    private static class TestChecker implements SessionCrossChecker<MyAccount> {
        private final Set<String> revokedTokens = Sets.newHashSet();

        public TestChecker addRevokedToken(String token) {
            revokedTokens.add(token);
            return this;
        }

        // ---------- Data verification --------------

        @Override
        public void verifySessionData(MyAccount sessionData) {
            Validatec.greaterThan(sessionData.userID, 1000000L, ArithmeticException::new);
            Validatec.length(sessionData.userName, 10, ArithmeticException.class);
            Validatec.length(sessionData.email, 10, ArithmeticException.class);
        }

        @Override
        public boolean isTokenRevoked(String token) {
            return revokedTokens.contains(token);
        }
    }

    private static class EncryptionKeysHolder {
        private static final int KEY_NUM_BYTES = 16;

        private static final JSONObject ENCRYPTION_KEYS = getEncryptionKeys();
        private static final byte[] HMAC_KEY = ENCRYPTION_KEYS.getString("HMAC_KEY_STRING").getBytes();
        private static final String PASSWORD = ENCRYPTION_KEYS.getString("ENCRYPTION_PASSWORD_STRING");
        private static final byte[] SALT = ENCRYPTION_KEYS.getString("ENCRYPTION_SALT_STRING").getBytes();

        // ------------- Private helpers ---------------

        private static JSONObject getEncryptionKeys() {
            return new JSONObject().put("ENCRYPTION_PASSWORD_STRING", RandomStringUtils.random(KEY_NUM_BYTES))
                                   .put("ENCRYPTION_SALT_STRING", RandomStringUtils.random(KEY_NUM_BYTES))
                                   .put("HMAC_KEY_STRING", RandomStringUtils.random(KEY_NUM_BYTES))
                                   .put("KEY_NUM_BITS", KEY_NUM_BYTES * 8);
        }
    }
}
