package org.rajivprab.sava.threading;

import com.google.common.collect.Range;
import org.junit.Assert;
import org.junit.Test;
import org.rajivprab.cava.ThreadUtilc;
import org.rajivprab.cava.exception.ExecutionExceptionc;
import org.rajivprab.sava.TestBase;
import org.rajivprab.sava.rest.IllegalUserInputException;

import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.IntStream;

import static com.google.common.truth.Truth.assertThat;

/**
 * Unit tests to ensure that ThreadScheduler is able to parallelize tasks appropriately
 * <p>
 * Created by rprabhakar on 10/22/15.
 */
public class ThreadHelperTest extends TestBase {
    private static final Duration SLEEP_DURATION = Duration.ofSeconds(1);
    private static final Duration TASK_SUBMIT_LATENCY = Duration.ofMillis(15);

    // ----------------- Basic Functionality: execute-runnable ---------------

    @Test
    public void execute_runnable_track() {
        ThreadHelper helper = getThreadHelper(true);
        checkRunTime(() -> executeSleep(helper), Duration.ZERO, TASK_SUBMIT_LATENCY);
        checkRunTime(helper::checkTasks, SLEEP_DURATION);
        checkRunTime(helper::close, Duration.ZERO, Duration.ofMillis(3));
    }

    @Test
    public void execute_runnable_noTrack() {
        ThreadHelper helper = getThreadHelper(false);
        checkRunTime(() -> executeSleep(helper), Duration.ZERO, TASK_SUBMIT_LATENCY);
        checkRunTime(helper::close, SLEEP_DURATION);
    }

    @Test
    public void execute_runnable_track_isShutDown_shouldRunOnMainThread() {
        ThreadHelper helper = getThreadHelper(true);
        helper.close();
        checkRunTime(() -> executeSleep(helper), SLEEP_DURATION);
    }

    @Test
    public void execute_runnable_noTrack_isShutDown_shouldRunOnMainThread() {
        ThreadHelper helper = getThreadHelper(false);
        helper.close();
        checkRunTime(() -> executeSleep(helper), SLEEP_DURATION);
    }

    // ----------------- Basic Functionality: execute-with-future runnable ---------------

    // TODO
    @Test
    public void executeFuture_runnable_track() {

    }

    @Test
    public void executeFuture_runnable_noTrack() {

    }

    @Test
    public void executeFuture_runnable_track_isShutDown() {

    }

    @Test
    public void executeFuture_runnable_noTrack_isShutDown() {

    }

    // ----------------- Basic Functionality: execute-with-future callable ---------------

    // TODO
    @Test
    public void executeFuture_callable_track() {

    }

    @Test
    public void executeFuture_callable_noTrack() {

    }

    @Test
    public void executeFuture_callable_track_isShutDown() {

    }

    @Test
    public void executeFuture_callable_noTrack_isShutDown() {

    }

    // ----------------- Stress tests -----------------

    @Test
    public void oneBatchOfTasks_allRunningInParallel_shouldFinishInOneRound() {
        assertThat(launchThreads(10, 5)).isIn(getTimeRange(1));
    }

    @Test
    public void twoBatchesOfTasks_secondBatchRunsOnceFirstBatchFinished_shouldFinishInTwoRounds() {
        assertThat(launchThreads(10, 11)).isIn(getTimeRange(2));
    }

    // ------------------- Exception handling ---------------------

    @Test
    public void exceptionInSideThread_doesNotInfluenceMainThread() {
        // No logging of exception. Only see the default JVM error message
        new Thread(() -> throwException(new IllegalUserInputException("test"))).start();
        ThreadUtilc.sleep(1000);    // Give executor time to run the thread
    }

    @Test
    public void execute_throwsNullPointerException_convertedToUserException_doesNotInfluenceMainThread() {
        // Should see IllegalUserInputException getting logged, in addition to JVM error message
        getThreadHelper().execute(() -> throwException(new NullPointerException()));
        ThreadUtilc.sleep(1000);     // Give executor time to run the thread
    }

    @Test
    public void executeFuture_hitsRunTimeException_getThrowsCause() {
        Future future = getThreadHelper().submit(() -> throwException(new ArithmeticException("test")));
        try {
            ThreadUtilc.get(future);
            Assert.fail();
        } catch (ExecutionExceptionc e) {
            assertThat(e.getCause()).isInstanceOf(ArithmeticException.class);
            assertThat(e.getCause()).hasMessageThat().isEqualTo("test");
        }
    }

    @Test
    public void executeFuture_hitsCheckedException_getThrowsCauseWithWrapper() {
        Future future = getThreadHelper().submit(() -> {throw new IOException("test");});
        try {
            ThreadUtilc.get(future);
            Assert.fail();
        } catch (ExecutionExceptionc e) {
            assertThat(e.getCause()).isInstanceOf(IOException.class);
            assertThat(e.getCause()).hasMessageThat().isEqualTo("test");
        }
    }

    @Test
    public void execute_hitsException_checkAllTasks_throwsCause() {
        ThreadHelper helper = getThreadHelper(true);
        helper.execute(() -> throwException(new ArithmeticException("test")));
        try {
            helper.checkTasks();
            Assert.fail();
        } catch (ExecutionExceptionc e) {
            assertThat(e.getCause()).isInstanceOf(ArithmeticException.class);
            assertThat(e.getCause()).hasMessageThat().isEqualTo("test");
        }
    }

    // ------------------ Misc functionality --------------

    @Test
    public void checkAllTasks_calledWithoutTrackAllTasks_shouldThrowError() {
        try {
            getThreadHelper().checkTasks();
            Assert.fail("Should have failed");
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().contains("checkTasks called without trackTasks being set");
        }
    }


    // ----------------------------- shutdown/termination ------------

    @Test
    public void shutdown_successfullyTerminated() {
        ThreadHelper helper = getThreadHelper();

        checkRunTime(() -> {
            executeSleep(helper);
            helper.shutdown();
        }, Duration.ZERO, TASK_SUBMIT_LATENCY);
        assertThat(helper.isShutdown()).isTrue();
        assertThat(helper.isTerminated()).isFalse();

        checkRunTime(() -> {
            boolean terminated = helper.awaitTermination(SLEEP_DURATION.multipliedBy(10));
            assertThat(terminated).isTrue();
        }, SLEEP_DURATION);
        assertThat(helper.isTerminated()).isTrue();
    }

    // -------------------- Private helpers --------------------------

    private static void executeSleep(ThreadHelper helper) {
        helper.execute(() -> ThreadUtilc.sleep(SLEEP_DURATION));
    }

    private static void throwException(RuntimeException e) { throw e; }

    private static Range<Duration> getTimeRange(int numBatches) {
        Duration minSleep = SLEEP_DURATION.multipliedBy(numBatches);
        return Range.open(minSleep.multipliedBy(99).dividedBy(100),
                          minSleep.multipliedBy(120).dividedBy(100));
    }

    private static Duration launchThreads(int numThreads, int numTasks) {
        ThreadHelper helper = getThreadHelper(numThreads, false);
        return getRunTime(() -> {
            IntStream.range(0, numTasks).forEach(i -> executeSleep(helper));
            helper.close();
        });
    }

    private static void checkRunTime(Runnable runnable, Duration minTime) {
        checkRunTime(runnable, minTime.multipliedBy(99).dividedBy(100),
                     minTime.multipliedBy(120).dividedBy(100));
    }

    private static void checkRunTime(Runnable runnable, Duration minTime, Duration maxTime) {
        assertThat(getRunTime(runnable)).isIn(Range.closed(minTime, maxTime));
    }

    private static Duration getRunTime(Runnable runnable) {
        long controlStart = System.nanoTime();
        long start = System.nanoTime();
        runnable.run();
        long end = System.nanoTime();
        return Duration.ofNanos((end - start) - (start - controlStart));
    }

    private static ThreadHelper getThreadHelper() {
        return getThreadHelper(false);
    }

    private static ThreadHelper getThreadHelper(boolean trackTasks) {
        return getThreadHelper(5, trackTasks);
    }

    private static ThreadHelper getThreadHelper(int numThreads, boolean trackTasks) {
        ThreadHelper helper = ThreadHelper.build(Executors.newFixedThreadPool(numThreads), trackTasks);
        assertThat(helper.isTrackingTasks()).isEqualTo(trackTasks);
        return helper;
    }
}
