package org.rajivprab.sava.email;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;
import org.rajivprab.sava.TestBase;

import static com.google.common.truth.Truth.assertThat;

/**
 * Integration test for the SendGrid wrapper.
 * Unfortunately, because an API key is needed and it's not safe to share the API key,
 * only Rajiv can run this from his computer
 * <p>
 * Created by rprabhakar on 3/26/16.
 */
public class SendGridsIT extends TestBase {
    private static final String ID = "2WgbluM_TS-Ez7VrtJMb3Q";
    private static final String APK = "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";

    private static final int UNSUB_GROUP1 = 549;
    private static final int UNSUB_GROUP2 = 551;
    private static final int UNSUB_GROUP3 = 553;

    private static final SendGrids CLIENT = SendGrids.client(APK);

    // Split up the email to make it harder for bots to find it
    private static final String RECEIVER = "whackri+sendgridtest1" + "@" + "gmai" + "l.com";
    private static final String SENDER = "whackri+sendgridtest2" + "@" + "gmai" + "l.com";

    private static final Pair<String, Integer> MANUAL_UNSUBBED = Pair.of(RECEIVER, UNSUB_GROUP3);

    @Test
    public void sendEmailWithoutBCC() {
        sendEmail(email().build());
    }

    @Test
    public void sendEmailWithBCC() {
        sendEmail(email().setBccEmail(SENDER).build());
    }

    @Test
    public void sendEmailToManuallyUnsubbedUser() {
        sendEmail(email().setSuppressionGroup(MANUAL_UNSUBBED.getRight())
                         .setBody("<h1>header1</h1> <p>this should never be sent out 1</p>").build());

        sendEmail(email().setBody("<h1>header1</h1> <p>this should be sent out 1</p>").build());
    }

    @Test
    public void unsubscribeAndResubscribe() {
        CLIENT.unsubscribe(RECEIVER, UNSUB_GROUP2);
        // TODO this email gets sent anyway. Investigate and build alternative test
        sendEmail(email().setBody("<h1>header2</h1> <p>this should never be sent out 2</p>").build());

        CLIENT.resubscribe(RECEIVER, UNSUB_GROUP2);
        sendEmail(email().setBody("<h1>header2</h1> <p>this should be sent out 2</p>").build());
    }

    private static void sendEmail(EmailInfo emailInfo) {
        boolean success = CLIENT.sendEmail(emailInfo);
        assertThat(success).isTrue();
    }

    private static EmailInfo.Builder email() {
        return EmailInfo.builder()
                        .setToEmail(RECEIVER)
                        .setTitle("email title")
                        .setBody("email body")
                        .setFromEmail(SENDER)
                        .setToName("testee")
                        .setFromName("tester")
                        .setSuppressionGroup(UNSUB_GROUP2);
    }
}
