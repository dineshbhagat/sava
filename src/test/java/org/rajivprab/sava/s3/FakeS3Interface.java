package org.rajivprab.sava.s3;

import com.google.common.base.Charsets;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.rajivprab.cava.IOUtilc;
import org.rajivprab.cava.exception.IOExceptionc;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * A fake S3Interface, which operates purely in-memory
 *
 * Created by rajivprab on 5/29/17.
 */
public class FakeS3Interface implements S3Interface {
    private final Table<String, String, String> contents;

    public FakeS3Interface() {
        this(HashBasedTable.create());
    }

    // Uses the provided table as its underlying data-store.
    // Any changes made to the table after the constructor, will be mirrored in the fake's behavior
    public FakeS3Interface(Table<String, String, String> contents) {
        this.contents = contents;
    }

    @Override
    public InputStream getS3Stream(String bucketName, String key) {
        return IOUtils.toInputStream(getFullContents(bucketName, key), Charsets.UTF_8);
    }

    @Override
    public void writeToS3(String bucketName, String key, InputStream inputStream) {
        contents.put(bucketName, key, IOUtilc.toString(inputStream));
    }

    @Override
    public String getFullContents(String bucketName, String key) {
        return contents.get(bucketName, key);
    }

    @Override
    public void downloadFile(String bucketName, String key, File outputFile) {
        try {
            FileUtils.write(outputFile, getFullContents(bucketName, key), Charsets.UTF_8);
        } catch (IOException e) {
            throw new IOExceptionc(e);
        }
    }

    @Override
    public String getURL(String bucketName, String key) {
        throw new UnsupportedOperationException();
    }
}
